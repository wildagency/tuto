var iScrollPos = 0;
$(window).scroll(function () {
    var wh = $(window).height();
    if ($(window).scrollTop() >= 80) {
        var iCurScrollPos = $(this).scrollTop();
        if (iCurScrollPos > iScrollPos) {
            $('.header-wrap').removeClass("active");
            $('.fixed-navwrap').addClass("top");
        } else {
            $('.header-wrap').addClass("active");
            $('.fixed-navwrap').removeClass("top");
        }
        iScrollPos = iCurScrollPos;
    } else {
        $('.header-wrap').addClass("active");
        $('.fixed-navwrap').removeClass("top");
    }

    let scroll_pos = $(this).scrollTop();
    effets(scroll_pos, wh);


    $("#navbarSupportedContent").removeClass('show')
    $("#navbarSupportedContent").removeClass('collapse')
    $("#navbar-toggler").attr('aria-expanded', "false")
    $("#navbar-toggler").removeClass('collapsed')

});
$(window).on('resize', function () {
    builder();
});

$(document).ready(function () {
    builder();
    var wh = $(window).height();
    let scroll_pos = $(this).scrollTop();
    effets(scroll_pos, wh);

    $(".has-sub-menu").hover(function () {
        let h = $(this).find(".sub-menu").outerHeight();
        $(".sub-menu-bg").height(h + "px")
        $(this).find(".sub-menu").css("display", "block");
    }, function () {
        $(".sub-menu-bg").height("0px")
        $(this).find(".sub-menu").css("display", "none");
    });

    $('.section-title').each(function () {
        let text = $(this).text();
        let res = text.split(" ");
        let str = "<strong>" + res[0] + "</strong>";
        for (let i = 1; i < res.length; i++) {
            str += " " + res[i]
        }
        $(this).html(str);
    });

});
function effets(scroll_pos, wh) {
    $(".section-title").each(function () {
        var oTop = $(this).offset().top,
            oBottom = $(this).offset().top + $(this).height();
        var spBottom = scroll_pos + wh;

        if (oTop < spBottom && scroll_pos < oBottom) {
            $(this).addClass("effect");
        } else {
            $(this).removeClass("effect");
        }
    });
}

function builder() {
    var wh = $(window).height(); //window height
    var ww = $(window).width(); //window width
    var hh = $('.header-wrap').height(); //window height
    $('.project-head').height((wh - hh) + "px")

    $('.badiwrap').css('min-height', (wh - 80 - 34) + 'px');

    if (ww > 992) {
        // $("#product-menu-wrap").mousemove(function (event) {
        //     var fromLeft = event.pageX - $(this).offset().left;
        //     if ($("#product-menu").width() >= fromLeft && fromLeft > 15) {
        //         $('.navbar-brand').css({ "-webkit-transform": "translateX(" + (fromLeft - 15) + "px)" });
        //     }
        //     // console.log(fromLeft);
        // });
        // $('#product-menu-wrap').mouseout(function (event) {
        //     var fromLeft = $(this).offset().left;
        //     var activeLeft = $('.product-menu .active').offset().left;
        //     if ((activeLeft - fromLeft) <= 15) {
        //         $('.navbar-brand').css({ "-webkit-transform": "translateX(" + 0 + "px)" });
        //     } else {
        //         $('.navbar-brand').css({ "-webkit-transform": "translateX(" + (activeLeft - fromLeft) + "px)" });
        //     }
        // });
    }
}
