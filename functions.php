<?php

if (!defined('ABSPATH')) {
    exit;
}
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if (!function_exists('wild_setup')) {
    function wild_setup()
    {
        // thumb
        add_theme_support('post-thumbnails');
        update_option( 'large_size_w', 1300 );
        update_option( 'medium_size_w', 595 );
        update_option( 'medium_size_h', 0 );
        update_option( 'thumbnail_size_w', 160 );
        update_option( 'thumbnail_size_h', 160 );

        add_image_size('slider-full', 1920, 1080, true);
        add_image_size('slider-thumb', 395, 240, true);
        // add_image_size('slider-thumb', 270, 166, true);
        
        add_theme_support('admin-bar');
        add_theme_support('html5', array(
            // 'search-form',
            // 'comment-form',
            // 'comment-list',
            // 'gallery',
            // 'caption',
        ));
        add_theme_support( 'editor' );
        add_theme_support('post-formats', array(
            // 'aside',
            // 'gallery',
            // 'link',
            // 'image',
            // 'quote',
            // 'status',
            // 'video',
            // 'audio',
            // 'chat',
        ));

    }
} // wild_setup


add_action('after_setup_theme', 'wild_setup');

/* Include all includes from source(src) folder */
foreach (glob(get_template_directory() . '/src/*.php') as $filename) {
    include_once $filename;
}

//----------------------------------- Set title to post, page and contents  -------------------------------------------
function wpdocs_filter_wp_title($title, $sep)
{
    global $paged, $page;
    if (is_feed()) {
        return $title;
    }
    // Add the site name.
    $title .= get_bloginfo('name');
    // Add the site description for the home/front page.
    $site_description = get_bloginfo('description', 'display');
    if ($site_description && (is_home() || is_front_page())) {
        $title = "$title $sep $site_description";
    }
    // Add a page number if necessary.
    if ($paged >= 2 || $page >= 2) {
        $title = "$title $sep " . sprintf(__('Page %s', 'ardsec'), max($paged, $page));
    }
    return $title;
}
add_filter('wp_title', 'wpdocs_filter_wp_title', 10, 2);



// allow svg upload
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


//remove editor text tab
function my_editor_settings($settings) {
    $settings['quicktags'] = false;
    return $settings;
}

add_filter('wp_editor_settings', 'my_editor_settings');



if ( is_plugin_active( 'js_composer/js_composer.php' ) ) {
    // remove front editro 
    vc_disable_frontend();
}



function wpb_image_editor_default_to_gd( $editors ) {
    $gd_editor = 'WP_Image_Editor_GD';
    $editors = array_diff( $editors, array( $gd_editor ) );
    array_unshift( $editors, $gd_editor );
    return $editors;
}
add_filter( 'wp_image_editors', 'wpb_image_editor_default_to_gd' );



function my_login_logo_one() { 
    ?> 
    <style type="text/css"> 
    body.login div#login h1 a {
     background-image: url(/wp-content/themes/tutottheme/assets/images/logo-image.svg);  
    padding-bottom: 30px; 
    } 
    </style>
     <?php 
} add_action( 'login_enqueue_scripts', 'my_login_logo_one' );