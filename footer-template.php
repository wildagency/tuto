<div class='footer-wrap'>
  <div class='container-wide'>
    <div class='row'>

  
      <div class='col d-lg-none d-block'>
        <div class=' mt-3 mt-sm-0'>
        <p><?php echo about_get_option('ab_footer_text_'.pll_current_language('slug')); ?></p>
        <p><b><?php echo pll__('tel'); ?> </b> <?php echo about_get_option('ab_phone'); ?></p>
        <p><b><?php echo pll__('email'); ?></b> <?php echo about_get_option('ab_email'); ?></p>
          
          <a href="<?php echo about_get_option('ab_brochure'); ?>" download class="ffd-btn p-0">
            <div class='bt p-0'><?php echo pll__('download_brochure'); ?></div>
            <div class="darkside">
              <svg xmlns="http://www.w3.org/2000/svg" width="12.588" height="15.285" viewBox="0 0 12.588 15.285">
                <path id="ic_file_download_24px" d="M17.588,8.395h-3.6V3H8.6V8.395H5l6.294,6.294ZM5,16.487v1.8H17.588v-1.8Z" transform="translate(-5 -3)" />
              </svg>
            </div>
          </a>
          <?php if(!empty(about_get_option('ab_sl_facebook'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_facebook'); ?>" target="_blank" class="solink">
              <svg id="Group_240" data-name="Group 240" xmlns="http://www.w3.org/2000/svg" width="4.904" height="10.57" viewBox="0 0 4.904 10.57">
                <path id="Path_353" data-name="Path 353" d="M243.841,350.7h-1.474v5.279h-2.2V350.7H239.13v-1.86h1.039V347.63a2.614,2.614,0,0,1,.169-.954,1.854,1.854,0,0,1,.664-.882,2.319,2.319,0,0,1,1.377-.387l1.631.012v1.812h-1.184a.468.468,0,0,0-.3.1.508.508,0,0,0-.157.411v1.1h1.667Z" transform="translate(-239.13 -345.407)"/>
              </svg>
            </a>
          <?php } ?>
          <?php if(!empty(about_get_option('ab_sl_instagram'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_instagram'); ?>" target="_blank" class="solink">
              <svg id="_733614" data-name="733614" xmlns="http://www.w3.org/2000/svg" width="10.162" height="10.162" viewBox="0 0 10.162 10.162">
                <g id="Group_2" data-name="Group 2">
                  <g id="Group_1" data-name="Group 1">
                    <path id="Path_1" data-name="Path 1" d="M6.987,0H3.176A3.176,3.176,0,0,0,0,3.176V6.987a3.176,3.176,0,0,0,3.176,3.176H6.987a3.176,3.176,0,0,0,3.176-3.176V3.176A3.176,3.176,0,0,0,6.987,0ZM9.21,6.987A2.225,2.225,0,0,1,6.987,9.21H3.176A2.225,2.225,0,0,1,.953,6.987V3.176A2.225,2.225,0,0,1,3.176.953H6.987A2.225,2.225,0,0,1,9.21,3.176Z" />
                  </g>
                </g>
                <g id="Group_4" data-name="Group 4" transform="translate(2.541 2.541)">
                  <g id="Group_3" data-name="Group 3">
                    <path id="Path_2" data-name="Path 2" d="M130.541,128a2.541,2.541,0,1,0,2.541,2.541A2.541,2.541,0,0,0,130.541,128Zm0,4.128a1.588,1.588,0,1,1,1.588-1.588A1.59,1.59,0,0,1,130.541,132.128Z" transform="translate(-128 -128)" />
                  </g>
                </g>
                <g id="Group_6" data-name="Group 6" transform="translate(7.474 2.012)">
                  <g id="Group_5" data-name="Group 5">
                    <circle id="Ellipse_1" data-name="Ellipse 1" cx="0.339" cy="0.339" r="0.339" />
                  </g>
                </g>
              </svg>
            </a>
          <?php } ?>
          <?php if(!empty(about_get_option('ab_sl_twitter'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_twitter'); ?>" target="_blank" class="solink">
              <svg xmlns="http://www.w3.org/2000/svg" width="13.021" height="10.57" viewBox="0 0 13.021 10.57">
                <g id="Group_306" data-name="Group 306" transform="translate(0)">
                  <path id="Path_419" data-name="Path 419" d="M241.914,1143.5v.342a7.728,7.728,0,0,1-.491,2.676,7.973,7.973,0,0,1-1.457,2.438,7.225,7.225,0,0,1-2.378,1.784,7.758,7.758,0,0,1-3.255.684,7.615,7.615,0,0,1-2.17-.312,7.38,7.38,0,0,1-1.932-.877q.312.03.639.03a5.3,5.3,0,0,0,1.769-.3,5.539,5.539,0,0,0,1.546-.832,2.67,2.67,0,0,1-1.546-.542,2.575,2.575,0,0,1-.936-1.316,2.669,2.669,0,0,0,.491.044,2.751,2.751,0,0,0,.7-.089,2.575,2.575,0,0,1-1.085-.5,2.649,2.649,0,0,1-.766-.937,2.783,2.783,0,0,1-.29-1.182v-.045a2.505,2.505,0,0,0,1.219.342,2.689,2.689,0,0,1-.87-.959,2.612,2.612,0,0,1-.32-1.271,2.654,2.654,0,0,1,.357-1.338,7.624,7.624,0,0,0,5.5,2.794,3.09,3.09,0,0,1-.059-.61,2.6,2.6,0,0,1,.357-1.338,2.694,2.694,0,0,1,.974-.974,2.57,2.57,0,0,1,1.33-.364,2.645,2.645,0,0,1,1.947.848,5.2,5.2,0,0,0,1.695-.654,2.582,2.582,0,0,1-.453.855,2.753,2.753,0,0,1-.721.632,5.43,5.43,0,0,0,1.546-.431A5.591,5.591,0,0,1,241.914,1143.5Z" transform="translate(-230.23 -1140.849)"/>
                </g>
              </svg>
            </a>
          <?php } ?>
          <?php if(!empty(about_get_option('ab_sl_vimeo'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_vimeo'); ?>" target="_blank" class="solink">
              <svg id="Group_311" data-name="Group 311" xmlns="http://www.w3.org/2000/svg" width="12.105" height="10.57" viewBox="0 0 12.105 10.57">
                <path id="Path_424" data-name="Path 424" d="M158.357,1214.718q.429-.47.872-.886a10.486,10.486,0,0,1,1.177-.942,3.434,3.434,0,0,1,1.232-.554.989.989,0,0,1,.955.277,2.559,2.559,0,0,1,.54,1.08,13.924,13.924,0,0,1,.3,1.426q.166,1.038.249,1.37.332,1.3.512,1.786.249.637.491.637t.692-.589a12.361,12.361,0,0,0,.99-1.585,2.182,2.182,0,0,0,.263-.927,1.11,1.11,0,0,0-.194-.72.761.761,0,0,0-.581-.3,1.678,1.678,0,0,0-.914.291,3.29,3.29,0,0,1,.817-1.675,3.415,3.415,0,0,1,1.405-.969,2.854,2.854,0,0,1,1.544-.166,1.61,1.61,0,0,1,1.121.748,2.417,2.417,0,0,1,.221,1.744,9.016,9.016,0,0,1-1,2.727,14.885,14.885,0,0,1-1.634,2.326,17.845,17.845,0,0,1-1.7,1.744,12.371,12.371,0,0,1-1.26.983,1.359,1.359,0,0,1-1.08.222,1.878,1.878,0,0,1-.9-.5,2.89,2.89,0,0,1-.568-.789,10.57,10.57,0,0,1-.457-1.288q-.153-.443-.609-1.966-.4-1.287-.6-1.869a4.522,4.522,0,0,0-.4-1,.45.45,0,0,0-.5-.083,2.413,2.413,0,0,0-.581.291l-.277.18-.485-.637Z" transform="translate(-158.011 -1212.224)"/>
              </svg>
            </a>
          <?php } ?>
          <?php if(!empty(about_get_option('ab_sl_youtube'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_youtube'); ?>" target="_blank" class="solink">
              <svg xmlns="http://www.w3.org/2000/svg" width="15.494" height="10.883" viewBox="0 0 15.494 10.883">
                <g id="Group_324" data-name="Group 324" transform="translate(0 0)">
                  <path id="Path_437" data-name="Path 437" d="M245.764,1365.243q0,1.294-.018,1.613-.071,1.046-.142,1.525a3.738,3.738,0,0,1-.23.869,1.739,1.739,0,0,1-.425.673,2.043,2.043,0,0,1-1.081.568q-3.369.249-7.18.177-2.234-.036-3.182-.08a10.638,10.638,0,0,1-1.409-.133,2.074,2.074,0,0,1-1.046-.55,2.411,2.411,0,0,1-.5-.974,3.334,3.334,0,0,1-.133-.55q-.045-.284-.08-.851a36.586,36.586,0,0,1,0-4.574l.106-.868a4.116,4.116,0,0,1,.2-.851,1.943,1.943,0,0,1,.461-.709,2.259,2.259,0,0,1,1.046-.532,17.957,17.957,0,0,1,2.154-.142q1.711-.053,3.714-.053t3.714.062q1.711.063,2.172.133a2.157,2.157,0,0,1,.683.266,1.748,1.748,0,0,1,.523.461,3.213,3.213,0,0,1,.408.957,5.369,5.369,0,0,1,.177,1.01q0,.089.053.94C245.758,1363.842,245.764,1364.383,245.764,1365.243Zm-7.144.869q1.4-.709,1.986-1.028l-4.166-2.2v4.379Q237.166,1366.858,238.62,1366.111Z" transform="translate(-230.27 -1359.8)"/>
                </g>
              </svg>
            </a>
          <?php } ?>
          <?php if(!empty(about_get_option('ab_sl_linkedin'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_linkedin'); ?>" target="_blank" class="solink">
              <svg xmlns="http://www.w3.org/2000/svg" width="10.57" height="10.57" viewBox="0 0 10.57 10.57">
                <g id="Group_262" data-name="Group 262" transform="translate(0)">
                  <path id="Path_375" data-name="Path 375" d="M96.129,633.073a.774.774,0,0,1,.559.223.722.722,0,0,1,.231.537v9.06a.7.7,0,0,1-.231.532.771.771,0,0,1-.547.218H87.127a.769.769,0,0,1-.547-.218.7.7,0,0,1-.231-.532v-9.06a.721.721,0,0,1,.231-.537.773.773,0,0,1,.559-.223h8.99Zm-6.719,3.195a.842.842,0,0,0,0-1.184.886.886,0,0,0-.644-.235.908.908,0,0,0-.65.235.835.835,0,0,0-.006,1.184.868.868,0,0,0,.632.236h.012A.9.9,0,0,0,89.411,636.268Zm.146.888H87.965v4.772h1.592Zm5.747,2.029a2.306,2.306,0,0,0-.522-1.618,1.9,1.9,0,0,0-2.272-.254,1.873,1.873,0,0,0-.474.52v-.677H90.443q.012.278,0,2.549v2.222h1.592v-2.67a1.18,1.18,0,0,1,.049-.387.962.962,0,0,1,.292-.4.81.81,0,0,1,.535-.181.7.7,0,0,1,.62.3,1.4,1.4,0,0,1,.182.773v2.561H95.3Z" transform="translate(-86.349 -633.073)"/>
                </g>
              </svg>
            </a>
          <?php } ?>
            
        </div>
      </div>
      
      <div class='col d-lg-block d-none'>
        <div class='float-sm-left mt-3 mt-sm-0 d-none d-lg-block'>
          <?php echo about_get_option('ab_footer_text_'.pll_current_language('slug')); ?>
        </div>
        <div class='float-sm-right mt-3 mt-sm-0'>
          <b><?php echo pll__('tel'); ?> </b> <?php echo about_get_option('ab_phone'); ?>
          <b><?php echo pll__('email'); ?></b> <?php echo about_get_option('ab_email'); ?>
          
          <a href="<?php echo about_get_option('ab_brochure'); ?>" download class="ffd-btn">
            <div class='bt'><?php echo pll__('download_brochure'); ?></div>
            <div class="darkside">
              <svg xmlns="http://www.w3.org/2000/svg" width="12.588" height="15.285" viewBox="0 0 12.588 15.285">
                <path id="ic_file_download_24px" d="M17.588,8.395h-3.6V3H8.6V8.395H5l6.294,6.294ZM5,16.487v1.8H17.588v-1.8Z" transform="translate(-5 -3)" />
              </svg>
            </div>
          </a>
          <?php if(!empty(about_get_option('ab_sl_facebook'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_facebook'); ?>" target="_blank" class="solink">
              <svg id="Group_240" data-name="Group 240" xmlns="http://www.w3.org/2000/svg" width="4.904" height="10.57" viewBox="0 0 4.904 10.57">
                <path id="Path_353" data-name="Path 353" d="M243.841,350.7h-1.474v5.279h-2.2V350.7H239.13v-1.86h1.039V347.63a2.614,2.614,0,0,1,.169-.954,1.854,1.854,0,0,1,.664-.882,2.319,2.319,0,0,1,1.377-.387l1.631.012v1.812h-1.184a.468.468,0,0,0-.3.1.508.508,0,0,0-.157.411v1.1h1.667Z" transform="translate(-239.13 -345.407)"/>
              </svg>
            </a>
          <?php } ?>
          <?php if(!empty(about_get_option('ab_sl_instagram'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_instagram'); ?>" target="_blank" class="solink">
              <svg id="_733614" data-name="733614" xmlns="http://www.w3.org/2000/svg" width="10.162" height="10.162" viewBox="0 0 10.162 10.162">
                <g id="Group_2" data-name="Group 2">
                  <g id="Group_1" data-name="Group 1">
                    <path id="Path_1" data-name="Path 1" d="M6.987,0H3.176A3.176,3.176,0,0,0,0,3.176V6.987a3.176,3.176,0,0,0,3.176,3.176H6.987a3.176,3.176,0,0,0,3.176-3.176V3.176A3.176,3.176,0,0,0,6.987,0ZM9.21,6.987A2.225,2.225,0,0,1,6.987,9.21H3.176A2.225,2.225,0,0,1,.953,6.987V3.176A2.225,2.225,0,0,1,3.176.953H6.987A2.225,2.225,0,0,1,9.21,3.176Z" />
                  </g>
                </g>
                <g id="Group_4" data-name="Group 4" transform="translate(2.541 2.541)">
                  <g id="Group_3" data-name="Group 3">
                    <path id="Path_2" data-name="Path 2" d="M130.541,128a2.541,2.541,0,1,0,2.541,2.541A2.541,2.541,0,0,0,130.541,128Zm0,4.128a1.588,1.588,0,1,1,1.588-1.588A1.59,1.59,0,0,1,130.541,132.128Z" transform="translate(-128 -128)" />
                  </g>
                </g>
                <g id="Group_6" data-name="Group 6" transform="translate(7.474 2.012)">
                  <g id="Group_5" data-name="Group 5">
                    <circle id="Ellipse_1" data-name="Ellipse 1" cx="0.339" cy="0.339" r="0.339" />
                  </g>
                </g>
              </svg>
            </a>
          <?php } ?>
          <?php if(!empty(about_get_option('ab_sl_twitter'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_twitter'); ?>" target="_blank" class="solink">
              <svg xmlns="http://www.w3.org/2000/svg" width="13.021" height="10.57" viewBox="0 0 13.021 10.57">
                <g id="Group_306" data-name="Group 306" transform="translate(0)">
                  <path id="Path_419" data-name="Path 419" d="M241.914,1143.5v.342a7.728,7.728,0,0,1-.491,2.676,7.973,7.973,0,0,1-1.457,2.438,7.225,7.225,0,0,1-2.378,1.784,7.758,7.758,0,0,1-3.255.684,7.615,7.615,0,0,1-2.17-.312,7.38,7.38,0,0,1-1.932-.877q.312.03.639.03a5.3,5.3,0,0,0,1.769-.3,5.539,5.539,0,0,0,1.546-.832,2.67,2.67,0,0,1-1.546-.542,2.575,2.575,0,0,1-.936-1.316,2.669,2.669,0,0,0,.491.044,2.751,2.751,0,0,0,.7-.089,2.575,2.575,0,0,1-1.085-.5,2.649,2.649,0,0,1-.766-.937,2.783,2.783,0,0,1-.29-1.182v-.045a2.505,2.505,0,0,0,1.219.342,2.689,2.689,0,0,1-.87-.959,2.612,2.612,0,0,1-.32-1.271,2.654,2.654,0,0,1,.357-1.338,7.624,7.624,0,0,0,5.5,2.794,3.09,3.09,0,0,1-.059-.61,2.6,2.6,0,0,1,.357-1.338,2.694,2.694,0,0,1,.974-.974,2.57,2.57,0,0,1,1.33-.364,2.645,2.645,0,0,1,1.947.848,5.2,5.2,0,0,0,1.695-.654,2.582,2.582,0,0,1-.453.855,2.753,2.753,0,0,1-.721.632,5.43,5.43,0,0,0,1.546-.431A5.591,5.591,0,0,1,241.914,1143.5Z" transform="translate(-230.23 -1140.849)"/>
                </g>
              </svg>
            </a>
          <?php } ?>
          <?php if(!empty(about_get_option('ab_sl_vimeo'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_vimeo'); ?>" target="_blank" class="solink">
              <svg id="Group_311" data-name="Group 311" xmlns="http://www.w3.org/2000/svg" width="12.105" height="10.57" viewBox="0 0 12.105 10.57">
                <path id="Path_424" data-name="Path 424" d="M158.357,1214.718q.429-.47.872-.886a10.486,10.486,0,0,1,1.177-.942,3.434,3.434,0,0,1,1.232-.554.989.989,0,0,1,.955.277,2.559,2.559,0,0,1,.54,1.08,13.924,13.924,0,0,1,.3,1.426q.166,1.038.249,1.37.332,1.3.512,1.786.249.637.491.637t.692-.589a12.361,12.361,0,0,0,.99-1.585,2.182,2.182,0,0,0,.263-.927,1.11,1.11,0,0,0-.194-.72.761.761,0,0,0-.581-.3,1.678,1.678,0,0,0-.914.291,3.29,3.29,0,0,1,.817-1.675,3.415,3.415,0,0,1,1.405-.969,2.854,2.854,0,0,1,1.544-.166,1.61,1.61,0,0,1,1.121.748,2.417,2.417,0,0,1,.221,1.744,9.016,9.016,0,0,1-1,2.727,14.885,14.885,0,0,1-1.634,2.326,17.845,17.845,0,0,1-1.7,1.744,12.371,12.371,0,0,1-1.26.983,1.359,1.359,0,0,1-1.08.222,1.878,1.878,0,0,1-.9-.5,2.89,2.89,0,0,1-.568-.789,10.57,10.57,0,0,1-.457-1.288q-.153-.443-.609-1.966-.4-1.287-.6-1.869a4.522,4.522,0,0,0-.4-1,.45.45,0,0,0-.5-.083,2.413,2.413,0,0,0-.581.291l-.277.18-.485-.637Z" transform="translate(-158.011 -1212.224)"/>
              </svg>
            </a>
          <?php } ?>
          <?php if(!empty(about_get_option('ab_sl_youtube'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_youtube'); ?>" target="_blank" class="solink">
              <svg xmlns="http://www.w3.org/2000/svg" width="15.494" height="10.883" viewBox="0 0 15.494 10.883">
                <g id="Group_324" data-name="Group 324" transform="translate(0 0)">
                  <path id="Path_437" data-name="Path 437" d="M245.764,1365.243q0,1.294-.018,1.613-.071,1.046-.142,1.525a3.738,3.738,0,0,1-.23.869,1.739,1.739,0,0,1-.425.673,2.043,2.043,0,0,1-1.081.568q-3.369.249-7.18.177-2.234-.036-3.182-.08a10.638,10.638,0,0,1-1.409-.133,2.074,2.074,0,0,1-1.046-.55,2.411,2.411,0,0,1-.5-.974,3.334,3.334,0,0,1-.133-.55q-.045-.284-.08-.851a36.586,36.586,0,0,1,0-4.574l.106-.868a4.116,4.116,0,0,1,.2-.851,1.943,1.943,0,0,1,.461-.709,2.259,2.259,0,0,1,1.046-.532,17.957,17.957,0,0,1,2.154-.142q1.711-.053,3.714-.053t3.714.062q1.711.063,2.172.133a2.157,2.157,0,0,1,.683.266,1.748,1.748,0,0,1,.523.461,3.213,3.213,0,0,1,.408.957,5.369,5.369,0,0,1,.177,1.01q0,.089.053.94C245.758,1363.842,245.764,1364.383,245.764,1365.243Zm-7.144.869q1.4-.709,1.986-1.028l-4.166-2.2v4.379Q237.166,1366.858,238.62,1366.111Z" transform="translate(-230.27 -1359.8)"/>
                </g>
              </svg>
            </a>
          <?php } ?>
          <?php if(!empty(about_get_option('ab_sl_linkedin'))){ ?>
            <a href="<?php echo about_get_option('ab_sl_linkedin'); ?>" target="_blank" class="solink">
              <svg xmlns="http://www.w3.org/2000/svg" width="10.57" height="10.57" viewBox="0 0 10.57 10.57">
                <g id="Group_262" data-name="Group 262" transform="translate(0)">
                  <path id="Path_375" data-name="Path 375" d="M96.129,633.073a.774.774,0,0,1,.559.223.722.722,0,0,1,.231.537v9.06a.7.7,0,0,1-.231.532.771.771,0,0,1-.547.218H87.127a.769.769,0,0,1-.547-.218.7.7,0,0,1-.231-.532v-9.06a.721.721,0,0,1,.231-.537.773.773,0,0,1,.559-.223h8.99Zm-6.719,3.195a.842.842,0,0,0,0-1.184.886.886,0,0,0-.644-.235.908.908,0,0,0-.65.235.835.835,0,0,0-.006,1.184.868.868,0,0,0,.632.236h.012A.9.9,0,0,0,89.411,636.268Zm.146.888H87.965v4.772h1.592Zm5.747,2.029a2.306,2.306,0,0,0-.522-1.618,1.9,1.9,0,0,0-2.272-.254,1.873,1.873,0,0,0-.474.52v-.677H90.443q.012.278,0,2.549v2.222h1.592v-2.67a1.18,1.18,0,0,1,.049-.387.962.962,0,0,1,.292-.4.81.81,0,0,1,.535-.181.7.7,0,0,1,.62.3,1.4,1.4,0,0,1,.182.773v2.561H95.3Z" transform="translate(-86.349 -633.073)"/>
                </g>
              </svg>
            </a>
          <?php } ?>
            
        </div>
        <div class='float-md-right mt-3 mt-sm-0 d-block d-lg-none'>
          <?php echo about_get_option('ab_footer_text_'.pll_current_language('slug')); ?>
        </div>
      </div>


    </div>
  </div>
</div>