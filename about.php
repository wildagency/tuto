<?php get_header(); ?>

<div class="badiwrap">
	<div class="content pb-0">
		<div class='container-wide'>
			<section>
				<div class='row'>
					<div class="col-lg-5 d-flex align-items-center">
						<img src="<?php echo about_get_option('ab_history_image');?>" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300" />
					</div>
					<div class="col-lg-7 d-flex align-items-center">
						<div class='pl-md-4 mt-4 mt-lg-0 tidp30' data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="800" >
							<h2 class="section-title"><?php echo pll__('history'); ?></h2>
							<div class='column2'><?php echo wpautop(about_get_option('ab_history_text_'.pll_current_language('slug'))); ?>		</div>			
						</div>
					</div>
				</div>
			</section>
			<!-- <section>
				<div class='row'>
					<div class="col-md-8 d-flex align-items-center">
						<div data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="800" >
							<h2 class="section-title"><?php echo pll__('vision'); ?></h2>
							<?php echo wpautop(about_get_option('ab_mission_text_'.pll_current_language('slug'))); ?>					
							<h2 class="section-title"><?php echo pll__('mission'); ?></h2>
							<?php echo wpautop(about_get_option('ab_vision_text_'.pll_current_language('slug'))); ?>	
						</div>
					</div>
					<div class="col-md-4">
						<img src="<?php echo about_get_option('ab_mission_image');?>" data-aos="fade-up"  data-aos-duration="1000" data-aos-delay="300"/>
					</div>
				</div>
			</section> -->
			<section>
				<div class='row'>
					<div class="col-md-7 d-flex align-items-center">
						<div class='pr-md-4 mt-4 mt-md-0 tidp30' data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="800" >
							<h2 class="section-title"><?php echo pll__('team'); ?></h2>
							<img src="<?php echo about_get_option('ab_team_image');?>" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300"  class="d-md-none d-block mb-4"/>
							<?php echo wpautop(about_get_option('ab_team_text_'.pll_current_language('slug'))); ?>					
						</div>
					</div>
					<div class="  col-md-5 d-md-block d-none">
						<div class=' d-flex align-items-center h-100'>
							<img src="<?php echo about_get_option('ab_team_image');?>" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300" />
						</div>
					</div>
				</div>
			</section>
			<section>
				<!-- <h2 class="section-title"  data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="800" ><?php echo pll__('founders'); ?></h2> -->
				<div class='row'>
					<?php
						$users = about_get_option('ab_founders_'.pll_current_language('slug'));
						foreach ($users as $key => $value) { ?>
							<div class="col-md-4 ">
								<div class='user'  data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="1000">
									<img src="<?php echo $value['user_image']; ?>" class="mb-4" />
									<div class='fs-18'><b><?php echo $value['user_name']; ?></b></div>
									<div class='fs-12 mb-4'><i><?php echo $value['user_pos']; ?></i></div>
									<p><?php echo $value['user_intro']; ?></p>
								</div>
							</div>
					<?php } ?>
				</div>

				<!-- <div class=""  data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="300">
					<div class='row mt-5 mb-5'>
						<div class="col text-center fs-18"><b class="section-title">Багын гишүүд</b></div>
					</div>
					<div class='row'>
						<?php
							$ab_members = about_get_option('ab_members_'.pll_current_language('slug'));
							foreach ($ab_members as $key => $value) { ?>
								<div class="col">
									<div class='text-center'>
										<div class='fs-18'><b><?php echo $value['user_name']; ?></b></div>
										<div class='fs-18 mb-4'><i><?php echo $value['user_pos']; ?></i></div>
										<p><?php echo $value['user_intro']; ?></p>
									</div>
								</div>
						<?php } ?>
					</div>
				</div>
				<div class=""  data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="300">
					<div class='row mt-5 mb-5'>
						<div class="col text-center fs-18"><b class="section-title">Түүхэн гишүүд</b></div>
					</div>
					<div class='row'>
						<?php
							$ab_historymembers = about_get_option('ab_historymembers_'.pll_current_language('slug'));
							foreach ($ab_historymembers as $key => $value) { ?>
								<div class="col">
									<div class='text-center'>
										<div class='fs-18'><b><?php echo $value['user_name']; ?></b></div>
										<div class='fs-18 mb-4'><i><?php echo $value['user_pos']; ?></i></div>
										<p><?php echo $value['user_intro']; ?></p>
									</div>
								</div>
						<?php } ?>
					</div>
				</div> -->
				
		

			</section>
			<section class="mt-0 mb-5"   data-aos="fade-up"   data-aos-duration="1000" data-aos-delay="300">
				<div class='mt-4 mb-5'>
					<?php
						$ab_membersall = about_get_option('ab_membersall_'.pll_current_language('slug'));
						echo wpautop($ab_membersall);
					?>
				</div>
				<!-- <h2 class="section-title"><?php echo pll__('ads'); ?></h2> -->
				<div class='row'>
					<div class='col-md-8 d-flex align-items-center'>
						<div class='pr-4'>
							<?php echo wpautop(about_get_option('ab_ads_text_'.pll_current_language('slug'))); ?>
						</div>
					</div>
					<div class="col-md-4 d-flex align-items-center justify-content-end">
						<img src="<?php echo about_get_option('ab_ads_image');?>" alt="" srcset="" class="" />
					</div>
				</div>
			</section>

			<section class="mb-5" id="awards">
				<div class="row">
					<div class="col-md-5 d-flex align-items-center">
						<img class="d-md-block d-none" src="<?php echo about_get_option('ab_award_image');?>"  data-aos="fade-up"   data-aos-duration="1000" data-aos-delay="300"/>
					</div>
					<div class="col-md-7 d-flex align-items-center">
						<div class='w-100'  data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="800">
							<h2 class="section-title"><?php echo pll__('awards'); ?></h2>
							<img class="d-md-none d-block mb-4" src="<?php echo about_get_option('ab_award_image');?>"  data-aos="fade-up"   data-aos-duration="1000" data-aos-delay="300"/>
							<?php echo wpautop(about_get_option('ab_award_text_'.pll_current_language('slug'))); ?>
						</div>

					</div>
				</div>
			</section>


			
			<?php $location = about_get_option('ab_location' ); ?>
			<section class="mt-5 pb-5 abf" data-aos="fade-up"   data-aos-duration="1000" data-aos-delay="300">
	
							

				<div class='row'>
					<div class='col-md-6'>
						<h2 class="section-title"><?php echo pll__('contact'); ?></h2>
						<table class="mb-4 cootable">
							<tr>
								<td><b><?php echo pll__('address'); ?></b></td>
								<td class="pl-4"><?php echo about_get_option('ab_address_'.pll_current_language('slug')); ?></td>
							</tr>
							<tr>
								<td><b><?php echo pll__('email'); ?></b></td>
								<td class="pl-4"><?php echo about_get_option('ab_email'); ?></td>
							</tr>
							<tr>
								<td><b><?php echo pll__('phone'); ?></b></td>
								<td class="pl-4"><?php echo about_get_option('ab_phone'); ?></td>
							</tr>
						</table>
						<table class="mb-4 cootable">
							<tr>
								<td><b><?php echo pll__('workingday'); ?></b></td>
								<td class="pl-4"><?php echo about_get_option('ab_workday_'.pll_current_language('slug')); ?></td>
							</tr>
							<tr>
								<td><b><?php echo pll__('saturday'); ?></b></td>
								<td class="pl-4"><?php echo about_get_option('ab_sunday_'.pll_current_language('slug')); ?></td>
							</tr>
							<tr>
								<td><b><?php echo pll__('sunday'); ?></b></td>
								<td class="pl-4"><?php echo about_get_option('ab_suterday_'.pll_current_language('slug')); ?></td>
							</tr>
						</table>

						

						<ul class="no-style mt-5">
							<?php if(!empty(about_get_option('ab_sl_facebook'))){ ?>
								<li class="d-inline-block mr-4"><a href="<?php echo about_get_option('ab_sl_facebook'); ?>" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/si-facebook.svg" alt="" /></a></li>
							<?php } ?>
							<?php if(!empty(about_get_option('ab_sl_instagram'))){ ?>
								<li class="d-inline-block mr-4"><a href="<?php echo about_get_option('ab_sl_instagram'); ?>" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/si-instagram.svg" alt="" /></a></li>
							<?php } ?>
							<?php if(!empty(about_get_option('ab_sl_twitter'))){ ?>
								<li class="d-inline-block mr-4"><a href="<?php echo about_get_option('ab_sl_twitter'); ?>" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/si-twitter.svg" alt="" /></a></li>
							<?php } ?>
							<?php if(!empty(about_get_option('ab_sl_vimeo'))){ ?>
								<li class="d-inline-block mr-4"><a href="<?php echo about_get_option('ab_sl_vimeo'); ?>" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/si-vimeo.svg" alt="" /></a></li>
							<?php } ?>
							<?php if(!empty(about_get_option('ab_sl_youtube'))){ ?>
								<li class="d-inline-block mr-4"><a href="<?php echo about_get_option('ab_sl_youtube'); ?>" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/si-youtube.svg" alt="" /></a></li>
							<?php } ?>
							<?php if(!empty(about_get_option('ab_sl_linkedin'))){ ?>
								<li class="d-inline-block mr-4"><a href="<?php echo about_get_option('ab_sl_linkedin'); ?>" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/si-linkedin.svg" alt="" /></a></li>
							<?php } ?>
								<li class="d-inline-block mr-4"><a href="<?php echo about_get_option('ab_jobrequest_url'); ?>" class="cwblack" target="_blank"><?php echo about_get_option('ab_jobrequest_text_'.pll_current_language('slug')); ?></a></li>
						</ul>
					</div>
					<div class='col-md-6'>
						<div class='pl-0 pl-md-5'>
							<?php if(!empty(about_get_option('ab_mapimage'))){ ?>
								<a href="<?php echo about_get_option('ab_mapimage_url'); ?>" target="_blank" >
									<img src="<?php echo esc_url(about_get_option('ab_mapimage')); ?>" class="w-100" alt="" srcset="" />
								</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>

<script>
	AOS.init();
</script>


<?php get_footer(); ?>


