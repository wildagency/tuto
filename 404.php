<?php get_header(); ?>

<div class='page-wrap'>
    <div class='page-wrap-box'>
        <!-- end page head -->
        <div class='page'>
            <div class='container' style="margin-top: 250px; margin-bottom: 250px">
                <h1 style="font-size: 60px">404</h1>
                <p><?php echo pll__('notfound'); ?></p>
            </div> <!-- end container -->
        </div> <!-- end page -->
    </div> <!-- end page wrap box -->
</div> <!-- end page wrap -->
    

<?php get_footer(); ?>


