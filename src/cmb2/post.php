<?php
add_action( 'cmb2_admin_init', 'fields_for_post' );

function fields_for_post() {
    $prefix = 'project_'; //post header
    
       
 

    $cmb = new_cmb2_box( array(
        'id'            => $prefix . 'cover',
        'title'         => __( 'Single Cover or Slider items', 'cmb2' ),
        'object_types'  => array( 'post' ), // Post type
        'context'      => 'normal', //  'normal', 'advanced', or 'side'
        'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
        'show_names'    => true, // Show field names on the left
        //'cmb_styles'    => false, // false to disable the CMB stylesheet
    ) );

    $cmb->add_field( array(
        'name' => __( 'Товч тайлбар', 'cmb2' ),
        'id'          => $prefix . 'intro',
        'type' => 'textarea_small',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Төслийн нэр', 'cmb2' ),
        'id'          => $prefix . 'name',
        'type' => 'text',
    ) );
    $cmb->add_field( array(
        'name' => __( 'Байшил:', 'cmb2' ),
        'id'          => $prefix . 'location',
        'type' => 'text',
    ) );
    $cmb->add_field( array(
        'name' => __( 'Xүчин чадал:', 'cmb2' ),
        'id'          => $prefix . 'capacity',
        'type' => 'text',
    ) );
    $cmb->add_field( array(
        'name' => __( 'Давxрын тоо:', 'cmb2' ),
        'id'          => $prefix . 'floors',
        'type' => 'text',
    ) );
    $cmb->add_field( array(
        'name' => __( 'Эзэмшил талбай:', 'cmb2' ),
        'id'          => $prefix . 'allarea',
        'type' => 'text',
    ) );
    $cmb->add_field( array(
        'name' => __( 'Барилгажиx талбай:', 'cmb2' ),
        'id'          => $prefix . 'buildingarea',
        'type' => 'text',
    ) );
    $cmb->add_field( array(
        'name' => __( 'Нийт талбай:', 'cmb2' ),
        'id'          => $prefix . 'emptyarea',
        'type' => 'text',
    ) );
    
    $cmb->add_field( array(
        'name' => __( 'Бүтэц:', 'cmb2' ),
        'id'          => $prefix . 'structure',
        'type' => 'text',
    ) );
    $cmb->add_field( array(
        'name' => __( 'Зургийн үе шат:', 'cmb2' ),
        'id'          => $prefix . 'picturelevel',
        'type' => 'text',
    ) );
    $cmb->add_field( array(
        'name' => __( 'Он:', 'cmb2' ),
        'id'          => $prefix . 'workarea',
        'type' => 'text',
    ) );
    $cmb->add_field( array(
        'name' => __( 'Он:', 'cmb2' ),
        'id'          => $prefix . 'workarea',
        'type' => 'text',
    ) );
    $cmb->add_field( array(
        'name' => __( 'Захиалагч:', 'cmb2' ),
        'id'          => $prefix . 'client',
        'type' => 'text',
    ) );


    $cmb->add_field( array(
        'name'    => 'Cover image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => $prefix . 'cover22',
        'type'    => 'file',
        // Optional:
        'options' => array(
            'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
            'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
            'type' => array(
            	'image/gif',
            	'image/jpeg',
            	'image/png',
            ),
        ),
        'preview_size' => 'large', // Image size to use when previewing in the admin.
    ) );

    $cmb->add_field( array(
        'name' => 'Sketche images',
        'id'          => $prefix . 'sketche_images',
        'type' => 'file_list',
        'preview_size' => array( 140, 40 ), // Default: array( 50, 50 )
        'query_args' => array( 'type' => 'image' ), // Only images attachment
        // Optional, override default text strings
        'text' => array(
            'add_upload_files_text' => 'Upload Images'
        ),
    ) );
    $cmb->add_field( array(
        'name' => 'Plan images',
        'id'          => $prefix . 'plan_images',
        'type' => 'file_list',
        'preview_size' => array( 140, 40 ), // Default: array( 50, 50 )
        'query_args' => array( 'type' => 'image' ), // Only images attachment
        // Optional, override default text strings
        'text' => array(
            'add_upload_files_text' => 'Upload Images'
        ),
    ) );
    $cmb->add_field( array(
        'name' => 'Model images',
        'id'          => $prefix . 'model_images',
        'type' => 'file_list',
        'preview_size' => array( 140, 40 ), // Default: array( 50, 50 )
        'query_args' => array( 'type' => 'image' ), // Only images attachment
        // Optional, override default text strings
        'text' => array(
            'add_upload_files_text' => 'Upload Images'
        ),
    ) );
    $cmb->add_field( array(
        'name' => 'Final images',
        'id'          => $prefix . 'final_images',
        'type' => 'file_list',
        'preview_size' => array( 140, 40 ), // Default: array( 50, 50 )
        'query_args' => array( 'type' => 'image' ), // Only images attachment
        // Optional, override default text strings
        'text' => array(
            'add_upload_files_text' => 'Upload Images'
        ),
    ) );
    
}