<?php
/**
 * CMB2 Theme Options
 *
 * @version 0.1.0
 * @package CMB2
 */
/**
 * Add a query var for template select, and and endpoint that sets that query var
 */
add_action( 'init', function () {
	global $wp, $wp_rewrite;
	$wp->add_query_var( 'template' );
	add_rewrite_endpoint( 'about', EP_ROOT );
	$wp_rewrite->add_rule( '([a-z]{2})/about(/(.*))?/?$', 'index.php?template=about', 'top' );
	$wp_rewrite->flush_rules();
} );

/**
 * Handle template redirect according the template being queried.
 */
add_action( 'template_redirect', function () {
	global $wp;
	$template = $wp->query_vars;

	if ( array_key_exists( 'template', $template ) && 'about' === $template[ 'template' ] ) {
		/* Make sure to set the 404 flag to false, and redirect to the specific page template. */
		global $wp_query;
		$wp_query->set( 'is_404', false );
		include( get_stylesheet_directory() . '/about.php' );
		exit;
	}
} );

/**
 * Admin Theme Options for About
 */
class About_Admin {

	/**
	 * Option key, and option page slug
	 *
	 * @var string
	 */
	private $key = '_options';

	/**
	 * Options page metabox id
	 *
	 * @var string
	 */
	private $metabox_id = '_option_metabox';

	/**
	 * Options Page title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * Options Page hook
	 *
	 * @var string
	 */
	protected $options_page = '';

	/**
	 * Holds an instance of the object
	 *
	 * @var About_Admin
	 * */
	private static $instance = null;

	/**
	 * Constructor
	 *
	 * @since 0.1.0
	 * @param String $prefix  Prefix name of options setting.
	 */
	private function __construct( $prefix ) {
		/* Set our title */
		$this->title		 = 'About';
		$this->key			 = $prefix . $this->key;
		$this->metabox_id	 = $prefix . $this->metabox_id;
	}

	/**
	 * Returns the running object
	 *
	 * @return About_Admin
	 * */
	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new About_Admin( 'about' );
			self::$instance->hooks();
		}
		return self::$instance;
	}

	/**
	 * Initiate our hooks
	 *
	 * @since 0.1.0
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_admin_init', array( $this, 'add_options_page_metabox' ) );
	}

	/**
	 * Register our setting to WP
	 *
	 * @since  0.1.0
	 */
	public function init() {
		register_setting( $this->key, $this->key );
	}

	/**
	 * Add menu options page
	 *
	 * @since 0.1.0
	 */
	public function add_options_page() {
		$this->options_page = add_menu_page( $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );

		/* Include CMB CSS in the head to avoid FOUC */
		add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
	}

	/**
	 * Admin page markup. Mostly handled by CMB2
	 *
	 * @since  0.1.0
	 */
	public function admin_page_display() {
		?>
		<div class="wrap cmb2-options-page <?php echo esc_html( $this->key ); ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
		</div>
		<?php
	}

	/**
	 * Add the options metabox to the array of metaboxes
	 *
	 * @since  0.1.0
	 */
	function add_options_page_metabox() {
		global $polylang;

		/* hook in our save notices */
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id}", array( $this, 'settings_notices' ), 10, 2 );

		$cmb = new_cmb2_box( array(
			'id'		 => $this->metabox_id,
			'hookup'	 => false,
			'cmb_styles' => false,
			'show_on'	 => array(
				/* These are important, don't remove */
				'key'	 => 'options-page',
				'value'	 => array( $this->key ),
			),
		) );

		/* Set our CMB2 fields */

		$cmb->add_field( array(
			'id'		 => 'ab_history_image',
			'name'		 => 'Түүх зураг',
			'type'		 => 'file',
			'attributes' => [ 'accept' => 'image/png,image/jpeg' ],
		) );
		if ( isset( $polylang ) ) {
			$languages = pll_languages_list();

			foreach ( $languages as $value ) {
				$cmb->add_field( array(
					'id'	 => 'ab_history_text_'.$value,
					'name'		 => 'Түүхийн дэлгэрэнгүй ('.$value.')',
					'type'	 => 'wysiwyg',
				) );
			}
		}

		$cmb->add_field( array(
			'id'		 => 'ab_mission_image',
			'name'		 => 'Алсын хараа, зорилго зураг',
			'type'		 => 'file',
			'attributes' => [ 'accept' => 'image/png,image/jpeg' ],
		) );
		if ( isset( $polylang ) ) {
			$languages = pll_languages_list();

			foreach ( $languages as $value ) {
				$cmb->add_field( array(
					'id'	 => 'ab_mission_text_'.$value,
					'name'		 => 'Алсын хараа ('.$value.')',
					'type'	 => 'wysiwyg',
				) );
			}
			foreach ( $languages as $value ) {
				$cmb->add_field( array(
					'id'	 => 'ab_vision_text_'.$value,
					'name'		 => 'Эрхэм зорилго ('.$value.')',
					'type'	 => 'wysiwyg',
				) );
			}
		}


		if ( isset( $polylang ) ) {
			$languages = pll_languages_list();
			$cmb->add_field( array(
				'id'		 => 'ab_team_image',
				'name'		 => 'Баг хамт олон зураг',
				'type'		 => 'file',
				'attributes' => [ 'accept' => 'image/png,image/jpeg' ],
			) );
			foreach ( $languages as $value ) {
				$cmb->add_field( array(
					'id'	 => 'ab_team_text_'.$value,
					'name'		 => 'Баг хамт олон ('.$value.')',
					'type'	 => 'wysiwyg',
				) );
			}

			foreach ( $languages as $value ) {
			

				$cmb->add_field( [
					'id'		 => 'ab_founders_'.$value,
					'name'		 => 'Үүсгэн байгуулагчид ('.$value.')',
					'type'		 => 'group',
					'repeatable' => true,
					'options'	 => [
						'sortable' => true
					],
					'fields'	 => [
						[
							'id'	 => 'user_name',
							'name'	 => 'Нэр (mn)',
							'type'	 => 'text',
						],
						[
							'id'	 => 'user_pos',
							'name'	 => 'Ажлын байр (en)',
							'type'	 => 'text',
						],
						[
							'id'	 => 'user_intro',
							'name'	 => 'Танилцуулга (mn)',
							'type'	 => 'textarea',
						],
						[
							'id'		 => 'user_image',
							'name'		 => 'Зураг',
							'type'		 => 'file',
							'attributes' => [ 'accept' => 'image/png,image/jpeg' ],
						],
					],
				] );
			}

			foreach ( $languages as $value ) {

				$cmb->add_field( [
					'id'		 => 'ab_membersall_'.$value,
					'name'		 => 'Баг хамт олон гишүүд ('.$value.')',
					'type'    => 'wysiwyg',
				] );
			}

			foreach ( $languages as $value ) {
			

				$cmb->add_field( [
					'id'		 => 'ab_members_'.$value,
					'name'		 => 'Багын гишүүд ('.$value.')',
					'type'		 => 'group',
					'repeatable' => true,
					'options'	 => [
						'sortable' => true
					],
					'fields'	 => [
						[
							'id'	 => 'user_name',
							'name'	 => 'Нэр (mn)',
							'type'	 => 'text',
						],
						[
							'id'	 => 'user_pos',
							'name'	 => 'Ажлын байр (en)',
							'type'	 => 'text',
						],
					],
				] );
			}

			foreach ( $languages as $value ) {
			

				$cmb->add_field( [
					'id'		 => 'ab_historymembers_'.$value,
					'name'		 => 'Түүхэн гишүүд ('.$value.')',
					'type'		 => 'group',
					'repeatable' => true,
					'options'	 => [
						'sortable' => true
					],
					'fields'	 => [
						[
							'id'	 => 'user_name',
							'name'	 => 'Нэр (mn)',
							'type'	 => 'text',
						],
						[
							'id'	 => 'user_pos',
							'name'	 => 'Ажлын байр (en)',
							'type'	 => 'text',
						],
					],
				] );
			}
		}
	

		$cmb->add_field( array(
			'id'		 => 'ab_award_image',
			'name'		 => 'Шагнал зураг',
			'type'		 => 'file',
			'attributes' => [ 'accept' => 'image/png,image/jpeg' ],
		) );
		if ( isset( $polylang ) ) {
			$languages = pll_languages_list();

			foreach ( $languages as $value ) {
				$cmb->add_field( array(
					'id'	 => 'ab_award_text_'.$value,
					'name'		 => 'Шагнал урамшуулал ('.$value.')',
					'type'	 => 'wysiwyg',
				) );
			}
		}
	
		$cmb->add_field( array(
			'id'		 => 'ab_ads_image',
			'name'		 => 'ADC зураг',
			'type'		 => 'file',
			'attributes' => [ 'accept' => 'image/png,image/jpeg' ],
		) );
		if ( isset( $polylang ) ) {
			$languages = pll_languages_list();

			foreach ( $languages as $value ) {
				$cmb->add_field( array(
					'id'	 => 'ab_ads_text_'.$value,
					'name'		 => 'ADS ('.$value.')',
					'type'	 => 'wysiwyg',
				) );
			}

			
			foreach ( $languages as $value ) {
				$cmb->add_field( array(
					'id'	 => 'ab_address_'.$value,
					'name'		 => 'Хаяг ('.$value.')',
					'type'	 => 'text',
				) );
			}
		}

		$cmb->add_field( array(
			'id'	 => 'ab_email',
			'name'		 => 'И-мэйл',
			'type'	 => 'text',
		) );
		
		$cmb->add_field( array(
			'id'	 => 'ab_phone',
			'name'		 => 'Утас',
			'type'	 => 'text',
		) );

		if ( isset( $polylang ) ) {
			$languages = pll_languages_list();

			foreach ( $languages as $value ) {
				$cmb->add_field( array(
					'id'	 => 'ab_workday_'.$value,
					'name'		 => 'Ажлын өдрүүд ('.$value.')',
					'type'	 => 'text',
				) );

				$cmb->add_field( array(
					'id'	 => 'ab_sunday_'.$value,
					'name'		 => 'Бямба ('.$value.')',
					'type'	 => 'text',
				) );
				$cmb->add_field( array(
					'id'	 => 'ab_suterday_'.$value,
					'name'		 => 'Ням ('.$value.')',
					'type'	 => 'text',
				) );
			}
		}
	
	

		$cmb->add_field( array(
			'name' => 'Location',
			'desc' => 'Drag the marker to set the exact location',
			'id' 	 => 'ab_location',
			'type' => 'pw_map',
			// 'split_values' => true, // Save latitude and longitude as two separate fields
			'api_key' => 'AIzaSyCR4idOlHvp8uYSyvoPsUET5x2FGX2OSSQ', // Google API Key
		) );

		$cmb->add_field( array(
			'id'	 => 'ab_sl_facebook',
			'name'		 => 'Facebook address',
			'type'	 => 'text_url',
		) );
		$cmb->add_field( array(
			'id'	 => 'ab_sl_instagram',
			'name'		 => 'Instagram address',
			'type'	 => 'text_url',
		) );
		$cmb->add_field( array(
			'id'	 => 'ab_sl_twitter',
			'name'		 => 'Twitter address',
			'type'	 => 'text_url',
		) );
		$cmb->add_field( array(
			'id'	 => 'ab_sl_vimeo',
			'name'		 => 'Vimeo address',
			'type'	 => 'text_url',
		) );
		$cmb->add_field( array(
			'id'	 => 'ab_sl_youtube',
			'name'		 => 'Youtube address',
			'type'	 => 'text_url',
		) );
		$cmb->add_field( array(
			'id'	 => 'ab_sl_linkedin',
			'name'		 => 'Linkedin address',
			'type'	 => 'text_url',
		) );
		$cmb->add_field( array(
			'id'	 => 'ab_contactform',
			'name'		 => 'Contact form shortcode',
			'type'	 => 'text',
		) );
		$cmb->add_field( array(
			'id'	 => 'ab_brochure',
			'name'		 => 'Brochure link',
			'type'		 => 'file',
		) );

		if ( isset( $polylang ) ) {
			$languages = pll_languages_list();

			foreach ( $languages as $value ) {
				$cmb->add_field( array(
					'id'	 => 'ab_footer_text_'.$value,
					'name'		 => 'Copyright text ('.$value.')',
					'type'		 => 'text',
				) );
			}

			
			foreach ( $languages as $value ) {
				$cmb->add_field( array(
					'id'	 => 'ab_jobrequest_text_'.$value,
					'name'		 => 'Ажилд орох хүсэлт илгээх ('.$value.')',
					'type'	 => 'text',
				) );
			}
		}

	
	
		$cmb->add_field( array(
			'id'	 => 'ab_jobrequest_url',
			'name'		 => 'Google form хаяг',
			'type'	 => 'text_url',
		) );
		$cmb->add_field( array(
			'id'	 => 'ab_mapimage',
			'name'		 => 'Contact map image',
			'type'		 => 'file',
		) );
		$cmb->add_field( array(
			'id'	 => 'ab_mapimage_url',
			'name'		 => 'Contact map URL',
			'type'		 => 'text_url',
		) );
	}

	/**
	 * Register settings notices for display
	 *
	 * @since  0.1.0
	 * @param  int   $object_id Option key.
	 * @param  array $updated   Array of updated fields.
	 * @return void
	 */
	public function settings_notices( $object_id, $updated ) {
		if ( $object_id !== $this->key || empty( $updated ) ) {
			return;
		}

		add_settings_error( $this->key . '-notices', '', 'Settings updated.', 'updated' );
		settings_errors( $this->key . '-notices' );
	}

	/**
	 * Public getter method for retrieving protected/private variables
	 *
	 * @since  0.1.0
	 * @param  string $field Field to retrieve.
	 * @throws Exception     When field value is invalid.
	 * @return mixed         Field value or exception is thrown.
	 */
	public function __get( $field ) {
		/* Allowed fields to retrieve */
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}

		throw new Exception( 'Invalid property: ' . $field );
	}

}

/**
 * Wrapper function around cmb2_get_option
 *
 * @since  0.1.0
 * @param  string $key Options array key.
 * @return mixed       Option value.
 */
function about_get_option( $key = '' ) {
	return cmb2_get_option( About_Admin::get_instance()->key, $key );
}

/* Get it started */
About_Admin::get_instance();
