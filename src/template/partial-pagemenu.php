<?php
$page_head_menu = get_post_meta(get_the_ID(),'page_head_menu')[0];
?>
<?php if(sizeof($page_head_menu)>0){?>
	<div class='page-head'>
		<div class='page-head-box'>
			<div class='page-nav'>
				<div class='page-nav-box'>
					<ul class="nav" id="nav-menu">
						<?php $isfirst = true; ?>
						<?php foreach ($page_head_menu as $key => $value) { ?>
							<li class="nav-item">
								<a class="nav-link <?php echo ($isfirst==true)?"active":"";?>" href="#_"
									data-section="#<?php echo $value['page_head_menu_item_url']; ?>"><?php echo $value['page_head_menu_item_name']; ?></a>
							</li>
							<?php $isfirst = false; ?>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php } ?>