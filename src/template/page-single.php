<?php
$cats = get_categories();


while (have_posts()) {
    the_post();

    $post_categories = wp_get_post_categories( get_the_ID() );
    $cats = array();
    foreach($post_categories as $c){
        $cat = get_category( $c );
        $cats[] = array( 'name' => $cat->name );
    }
    ?>
    <div class="badiwrap">
        <div class='project-head d-flex flex-column justify-content-end'
            style="background-image: url(<?php echo get_post_meta(get_the_ID(), 'project_cover22')[0]; ?>)">
            <div class='overlay'></div>
            <div class='head-detail' data-aos="fade-zoom-in"   data-aos-duration="2500" data-aos-delay="0">
                <div class='container'>
                    <div class='code'><b><?php the_title(); ?></b></div>
                    <div class='title'><?php echo get_post_meta(get_the_ID(), 'project_name')[0]; ?></div>
                    <div class='cat'><?php echo $cats[0]['name']; ?></div>
                    <div class='detail'>
                        <table>
                            <?php if(!empty(get_post_meta(get_the_ID(), 'project_location')[0])){ ?>
                                <tr>
                                    <td><b><?php echo pll__('location'); ?></b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'project_location')[0]; ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty(get_post_meta(get_the_ID(), 'project_capacity')[0])){ ?>
                                <tr>
                                    <td><b><?php echo pll__('capacity'); ?></b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'project_capacity')[0]; ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty(get_post_meta(get_the_ID(), 'project_floors')[0])){ ?>
                                <tr>
                                    <td><b><?php echo pll__('floors'); ?></b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'project_floors')[0]; ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty(get_post_meta(get_the_ID(), 'project_allarea')[0])){ ?>
                                <tr>
                                    <td><b><?php echo pll__('allarea'); ?></b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'project_allarea')[0]; ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty(get_post_meta(get_the_ID(), 'project_buildingarea')[0])){ ?>
                                <tr>
                                    <td><b><?php echo pll__('buildingarea'); ?></b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'project_buildingarea')[0]; ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty(get_post_meta(get_the_ID(), 'project_emptyarea')[0])){ ?>
                                <tr>
                                    <td><b><?php echo pll__('emptyarea'); ?></b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'project_emptyarea')[0]; ?></td>
                                </tr>
                            <?php } ?>
                           
                            <?php if(!empty(get_post_meta(get_the_ID(), 'project_structure')[0])){ ?>
                                <tr>
                                    <td><b><?php echo pll__('structure'); ?></b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'project_structure')[0]; ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty(get_post_meta(get_the_ID(), 'project_picturelevel')[0])){ ?>
                                <tr>
                                    <td><b><?php echo pll__('picturelevel'); ?></b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'project_picturelevel')[0]; ?></td>
                                </tr>
                            <?php } ?>

                            <?php if(!empty(get_post_meta(get_the_ID(), 'project_workarea')[0])){ ?>
                                <tr>
                                    <td><b><?php echo pll__('year'); ?></b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'project_workarea')[0]; ?></td>
                                </tr>
                            <?php } ?>

                            <?php if(!empty(get_post_meta(get_the_ID(), 'project_client')[0])){ ?>
                                <tr>
                                    <td><b><?php echo pll__('client'); ?></b></td>
                                    <td><?php echo get_post_meta(get_the_ID(), 'project_client')[0]; ?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class='container'>
                <?php $cnt = get_the_content();?>
                <?php if(!empty($cnt)){ ?>
                    <section class='tidp30'  data-aos="fade-zoom-in" data-aos-duration="1000" data-aos-delay="300" data-aos-anchor-placement="center-center">
                        <h2 class="section-title"><?php echo pll__('text'); ?></h2>
                        <?php
                        $introtext = get_post_meta(get_the_ID(), 'project_intro')[0];
                        if(empty($introtext)){
                            the_content();
                        }else{ ?>
                            <div class='withintro'>
                                <div class='introtext'><p><?php echo $introtext; ?> <a href="#_" onClick="showMore()" class="clink" ><?php echo pll__('readmore'); ?></a></p></div>
                                <div class='fulltext d-none'><?php the_content(); ?></div>
                            </div>
                        <?php } ?>
                    </section>
                <?php } ?>
                <?php if(!empty(get_post_meta(get_the_ID(), 'project_sketche_images')[0])){ ?>
                    <?php 
                        $sketch_images = get_post_meta(get_the_ID(), 'project_sketche_images')[0]; 
                        $sketch_images_arr = array();
                        foreach ($sketch_images as $key => $value) {
                            array_push($sketch_images_arr, array(
                                'full' => wp_get_attachment_image_src($key,'slider-full' ),
                                'thumb'=> wp_get_attachment_image_src($key,'slider-thumb' )
                            ));
                        }
                    ?>
                    <section  data-aos="fade-zoom-in"   data-aos-duration="1000" data-aos-delay="300" data-aos-anchor-placement="center-center">
                        <h2 class="section-title"><?php echo pll__('sketches'); ?></h2>
                        <!-- Swiper -->
                        <div class="swiper-container sketch-swiper">
                            <div class="swiper-wrapper" id="sketch-gallery">
                                <?php foreach ($sketch_images_arr  as $key => $value) { ?>
                                    <div class="swiper-slide" data-src="<?php echo $value['full'][0]; ?>">
                                        <a href="">
                                            <img src="<?php echo $value['thumb'][0]; ?>" alt="" />
                                        </a>
                                    </div>
                                <?php }?>
                            </div>
                        </div>

                        <!-- Initialize Swiper -->
                        <script>
                             var swiper2 = new Swiper('.sketch-swiper', {
                                    slidesPerView: 4,
                                    spaceBetween: 10,
                                    breakpoints: {
                                        768: {
                                            slidesPerView: 2
                                        }
                                    }
                                });

                            $(document).ready(() => {
                             
                                $('#sketch-gallery').lightGallery({
                                    thumbnail: true,
                                    download: false,
                                    thumbWidth: 80,
                                    currentPagerPosition: 'middle',
                                    toggleThumb: false,
                                    thumbContHeight: 80,
                                    fullScreen: false,
                                    autoplay: false,
                                    autoplayControls: false,
                                    share: false,
                                    zoom: false
                                });
                            })

                        </script>

                    </section>
                <?php } ?>
                <?php if(!empty(get_post_meta(get_the_ID(), 'project_plan_images')[0])){ ?>
                    <?php 
                        $plan_images = get_post_meta(get_the_ID(), 'project_plan_images')[0]; 
                        $plan_images_arr = array();
                        foreach ($plan_images as $key => $value) {
                            array_push($plan_images_arr, array(
                                'full' => wp_get_attachment_image_src($key,'slider-full' ),
                                'thumb'=> wp_get_attachment_image_src($key,'slider-thumb' )
                            ));
                        }
                    ?>
                    <section  data-aos="fade-zoom-in"   data-aos-duration="1000" data-aos-delay="300" data-aos-anchor-placement="center-center">
                        <h2 class="section-title"><?php echo pll__('plans'); ?></h2>
                        <!-- Swiper -->
                        <div class="swiper-container plans-swiper">
                            <div class="swiper-wrapper" id="plans-gallery">
                                <?php foreach ($plan_images_arr  as $key => $value) { ?>
                                    <div class="swiper-slide" data-src="<?php echo $value['full'][0]; ?>">
                                        <a href="">
                                            <img src="<?php echo $value['thumb'][0]; ?>" alt="" />
                                        </a>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                        <!-- Initialize Swiper -->
                        <script>
                             var swiper3 = new Swiper('.plans-swiper', {
                                    slidesPerView: 4,
                                    spaceBetween: 10,
                                    breakpoints: {
                                        768: {
                                            slidesPerView: 2
                                        }
                                    }
                                });
                            $(document).ready(() => {
                             
                                $('#plans-gallery').lightGallery({
                                    thumbnail: true,
                                    download: false,
                                    thumbWidth: 80,
                                    currentPagerPosition: 'middle',
                                    toggleThumb: false,
                                    thumbContHeight: 80,
                                    fullScreen: false,
                                    autoplay: false,
                                    autoplayControls: false,
                                    share: false,
                                    zoom: false
                                });
                            })
                        </script>
                    </section>
                <?php } ?>
                <?php if(!empty(get_post_meta(get_the_ID(), 'project_model_images')[0])){ ?>
                    <?php 
                        $model_images = get_post_meta(get_the_ID(), 'project_model_images')[0]; 
                        $model_images_arr = array();
                        foreach ($model_images as $key => $value) {
                            array_push($model_images_arr, array(
                                'full' => wp_get_attachment_image_src($key,'slider-full' ),
                                'thumb'=> wp_get_attachment_image_src($key,'slider-thumb' )
                            ));
                        }
                    ?>
                    <section  data-aos="fade-zoom-in"   data-aos-duration="1000" data-aos-delay="300" data-aos-anchor-placement="center-center">
                        <h2 class="section-title"><?php echo pll__('models'); ?></h2>
                        <!-- Swiper -->
                        <div class="swiper-container model-swiper">
                            <div class="swiper-wrapper" id="model-gallery">
                                <?php foreach ($model_images_arr  as $key => $value) { ?>
                                    <div class="swiper-slide" data-src="<?php echo $value['full'][0]; ?>">
                                        <a href="">
                                            <img src="<?php echo $value['thumb'][0]; ?>" alt="" />
                                        </a>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                        <!-- Initialize Swiper -->
                        <script>
                          var swiper4 = new Swiper('.model-swiper', {
                                    slidesPerView: 4,
                                    spaceBetween: 10,
                                    breakpoints: {
                                        768: {
                                            slidesPerView: 2
                                        }
                                    }
                                });
                            $(document).ready(() => {
                               
                                $('#model-gallery').lightGallery({
                                    thumbnail: true,
                                    download: false,
                                    thumbWidth: 80,
                                    currentPagerPosition: 'middle',
                                    toggleThumb: false,
                                    thumbContHeight: 80,
                                    fullScreen: false,
                                    autoplay: false,
                                    autoplayControls: false,
                                    share: false,
                                    zoom: false
                                });
                            })
                        </script>
                    </section>
                <?php } ?>
                <?php if(!empty(get_post_meta(get_the_ID(), 'project_final_images')[0])){ ?>
                    <?php 
                        $final_images = get_post_meta(get_the_ID(), 'project_final_images')[0]; 
                        $final_images_arr = array();
                        foreach ($final_images as $key => $value) {
                            array_push($final_images_arr, array(
                                'full' => wp_get_attachment_image_src($key,'slider-full' ),
                                'thumb'=> wp_get_attachment_image_src($key,'slider-thumb' )
                            ));
                        }
                    ?>
                    <section data-aos="fade-zoom-in"   data-aos-duration="1000" data-aos-delay="300" data-aos-anchor-placement="center-center">
                        <h2 class="section-title"><?php echo pll__('finalimages'); ?></h2>
                        <!-- Swiper -->
                        <div class="swiper-container final-swiper">
                            <div class="swiper-wrapper" id="final-gallery">
                                <?php foreach ($final_images_arr  as $key => $value) { ?>
                                    <div class="swiper-slide" data-src="<?php echo $value['full'][0]; ?>">
                                        <a href="">
                                            <img src="<?php echo $value['thumb'][0]; ?>" alt="" />
                                        </a>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                        <!-- Initialize Swiper -->
                        <script>
                       
                            $(document).ready(() => {
                                var swiper5 = new Swiper('.final-swiper', {
                                    slidesPerView: 4,
                                    spaceBetween: 10,
                                    breakpoints: {
                                        768: {
                                            slidesPerView: 2
                                        }
                                    }
                                });
                                $('#final-gallery').lightGallery({
                                    thumbnail: true,
                                    download: false,
                                    thumbWidth: 80,
                                    currentPagerPosition: 'middle',
                                    toggleThumb: false,
                                    thumbContHeight: 80,
                                    fullScreen: false,
                                    autoplay: false,
                                    autoplayControls: false,
                                    share: false,
                                    zoom: false
                                });
                            })
                        </script>
                    </section>
                <?php } ?>
                <!-- prev next button -->
                <div class='pn-btn row bs-m8'>
                    <div class='col bs-p8'>
                        <?php 
                        $prev_post = get_adjacent_post(false, '', true); 
                        if(!empty($prev_post)) { ?>
                            <a href="<?php echo get_permalink($prev_post->ID); ?>" title="<?php echo $prev_post->post_title; ?>" class="btn btn-link float-left btn-np">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                    <path id="ic_arrow_back_24px" d="M18,10.125H7.351l4.891-4.891L11,4,4,11l7,7,1.234-1.234L7.351,11.875H18Z" transform="translate(-4 -4)"/>
                                </svg>
                                <?php echo pll__('prevthez'); ?>
                            </a>
                        <?php } ?>
                        
                    </div>
                    <div class='col bs-p8'>
                        <?php 
                        $next_post = get_adjacent_post(false, '', false); 
                        if(!empty($next_post)) { ?>
                            <a href="<?php echo get_permalink($next_post->ID); ?>" title="<?php echo $next_post->post_title; ?>" class="btn btn-link float-right btn-np">
                                <?php echo pll__('nextthez'); ?>
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                    <path id="ic_arrow_forward_24px" d="M11,4,9.766,5.234l4.882,4.891H4v1.75H14.649L9.766,16.766,11,18l7-7Z" transform="translate(-4 -4)"/>
                                </svg>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        AOS.init();
    </script>
    <?php
    wp_reset_postdata();
} //end while
?>

<script>
    var swiper = new Swiper('.final-swiper', {
        slidesPerView: 4,
        spaceBetween: 10,
    });

    function showMore(){
        $('.introtext').addClass('d-none')
        $('.fulltext').removeClass('d-none')
    }
    $(document).ready(() => {


        var wh = $(window).height();
        var lastScrollTop = 0;
        let animate = 0
        $(window).scroll(function(event){
            var st = $(this).scrollTop();
            if (st > lastScrollTop && st < wh ){
                if(animate===0){
                    $('html, body').animate({
                        scrollTop: wh
                    }, 500)
                }
                animate++
            } else if(st == lastScrollTop) {
                //do nothing 
                //In IE this is an important condition because there seems to be some instances where the last scrollTop is equal to the new one
            } else {
                animate = 0
            }
            lastScrollTop = st;
        });
    })

</script>