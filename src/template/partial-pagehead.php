
<?php
$page_head_images = get_post_meta(get_the_ID(),'page_head_images')[0];
$page_head_contents = get_post_meta(get_the_ID(),'page_head_slider')[0];
?>
<?php if(sizeof($page_head_images)>1){?>
	<div class='head-cover'>
		<div class='container-wide'>
			<div class='swiper-container main-swiper'>
				<div class='swiper-wrapper'>
					<?php $slide_ct = 0; ?>
					<?php foreach ($page_head_images as $key => $value) { ?>
						<?php $bgUrl = wp_get_attachment_image_src( $key, 'cover-thumb' );?>
						<div class='swiper-slide'>
							<div class='head-cover-box'
								style='background-image: url(<?php echo esc_url($bgUrl[0]); ?>);'>
								<img src='<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ratio-7-2.png' class='w-100' alt=''>
								<div class='head-cover-content text-center d-flex align-items-center'>
									<div class='container'>
										<?php echo $page_head_contents[$slide_ct]['page_head_slide_content'];?>
										<?php if($page_head_contents[$slide_ct]['page_head_slide_btn_url'] && $page_head_contents[$slide_ct]['page_head_slide_btn_text']){ ?>
											<a href="<?php echo $page_head_contents[$slide_ct]['page_head_slide_btn_url'];?>" class='btn btn-outline-light'><?php echo $page_head_contents[$slide_ct]['page_head_slide_btn_text'];?></a>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						<?php $slide_ct++; ?>
					<?php } ?>
				</div>
				<!-- Add Arrows -->
				<div class="swiper-button-next swiper-button-white"></div>
				<div class="swiper-button-prev swiper-button-white"></div>
			</div>
			<script>
				var mainSwiper = new Swiper('.main-swiper', {
					spaceBetween: 0,
					effect: 'fade',
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',
					},
					autoplay: {
						delay: 8500,
						disableOnInteraction: false,
					},
				});
			</script>
		</div>
	</div>
<?php }else{ ?>
	<?php foreach ($page_head_images as $key => $value) { ?>
		<?php $bgUrl = wp_get_attachment_image_src( $key, 'cover-thumb' );?>
		<div class='head-cover without-title'>
			<div class='container-wide'>
				<div class='head-cover-box' style='background-image: url(<?php echo esc_url($bgUrl[0]); ?>);'>
					<img src='<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ratio-7-2.png' class='w-100' alt=''>
				</div>
			</div>
		</div>
	<?php } ?>
<?php }?>

