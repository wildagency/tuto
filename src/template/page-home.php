<?php
$categories = get_categories( array(
    'orderby' => 'id',
    'parent'  => 0
) );
 
$cats_list_html = "";
foreach ( $categories as $category ) {
    $cats_list_html .= "<li class='nav-item' onclick='activeitem(this)'><a class='nav-link' href='#' onclick='enablebycat(this)' data-catid='".$category->term_id."'>".$category->name."</a></li>";
}
?>
<div class="badiwrap">
    <div class='nav-wrap fixed-navwrap'  >
        <div class='container-wide' id="product-menu-wrap">
            <div class='d-lg-inline-block '>
                <ul class="nav product-menu" id="product-menu">
                    <li class="nav-item active" onclick='activeitem(this)'>
                        <a class="nav-link " href="#" onclick='enablebyall()'><?php echo pll__('all'); ?></a>
                    </li>
                    <?php echo $cats_list_html; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class='container-wide' style="margin-top: 84px">
        <div class='project-items'>
            <?php
            $args = array(
                'posts_per_page' => -1,
                'orderby' => 'rand',
                'order' => 'DESC',
            );
            // The Query
            $the_query = new WP_Query( $args );
            // The Loop
            if ( $the_query->have_posts() ) {
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    $post_cats_arr = wp_get_post_categories(get_the_ID());
                    $catName = get_cat_name($post_cats_arr[0]);
                    $post_cats_str = implode(', ', $post_cats_arr);
                    $project_name = get_post_meta(get_the_ID(), 'project_name')[0];
                    ?>
                    <div class='p-item-wrap d-none' data-cats="<?php echo $post_cats_str; ?>" data-showable="false" data-show="false" data-aos="fade-up"  data-aos-duration="1000" data-aos-delay="300">
                        <a href="<?php the_permalink(); ?>" class="p-item">
                            <div class="thumb-wrap" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium_size_w'); ?>">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/ratio-3-2.png" alt="" srcset="">
                            </div>
                            <div class='info  d-flex flex-column justify-content-center'>
                                <div class='code'><?php the_title(); ?></div>
                                <div class='title'>
                                    <?php echo $project_name; ?>
                                </div>
                                <div class='cat'><?php echo $catName; ?></div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
            } else {
                // no posts found
            }
            /* Restore original Post Data */
            wp_reset_postdata();
            ?>
        </div>
        <!-- <div class='loader d-flex justify-content-center'>
            <div icon="bubbles" class="spinner-balanced spinner spinner-bubbles">
                <svg viewBox="0 0 64 64">
                    <g stroke-width="0">
                        <circle cx="24" cy="0" transform="translate(32,32)" r="6.27678">
                            <animate attributeName="r" dur="750ms" values="8;7;6;5;4;3;2;1;8" repeatCount="indefinite">
                            </animate>
                        </circle>
                        <circle cx="16.970562748477143" cy="16.97056274847714" transform="translate(32,32)" r="7.27678">
                            <animate attributeName="r" dur="750ms" values="1;8;7;6;5;4;3;2;1" repeatCount="indefinite">
                            </animate>
                        </circle>
                        <circle cx="1.4695761589768238e-15" cy="24" transform="translate(32,32)" r="6.06255">
                            <animate attributeName="r" dur="750ms" values="2;1;8;7;6;5;4;3;2" repeatCount="indefinite">
                            </animate>
                        </circle>
                        <circle cx="-16.97056274847714" cy="16.970562748477143" transform="translate(32,32)"
                            r="1.27678">
                            <animate attributeName="r" dur="750ms" values="3;2;1;8;7;6;5;4;3" repeatCount="indefinite">
                            </animate>
                        </circle>
                        <circle cx="-24" cy="2.9391523179536475e-15" transform="translate(32,32)" r="2.27678">
                            <animate attributeName="r" dur="750ms" values="4;3;2;1;8;7;6;5;4" repeatCount="indefinite">
                            </animate>
                        </circle>
                        <circle cx="-16.970562748477143" cy="-16.97056274847714" transform="translate(32,32)"
                            r="3.27678">
                            <animate attributeName="r" dur="750ms" values="5;4;3;2;1;8;7;6;5" repeatCount="indefinite">
                            </animate>
                        </circle>
                        <circle cx="-4.408728476930472e-15" cy="-24" transform="translate(32,32)" r="4.27678">
                            <animate attributeName="r" dur="750ms" values="6;5;4;3;2;1;8;7;6" repeatCount="indefinite">
                            </animate>
                        </circle>
                        <circle cx="16.970562748477136" cy="-16.970562748477143" transform="translate(32,32)"
                            r="5.27678">
                            <animate attributeName="r" dur="750ms" values="7;6;5;4;3;2;1;8;7" repeatCount="indefinite">
                            </animate>
                        </circle>
                    </g>
                </svg>
            </div>
        </div> -->
    </div>
</div>
<script>
    AOS.init();
    $(window).scroll(function () {
        let bh = $('body').height();
        let wh = $(window).height();
        var scrollTop = this.scrollY;
        if((scrollTop+wh) >= bh){
            showbylimit();
        }
    });
    function enablebycat(e) {


        var catid = $(e).attr('data-catid');
        $(".p-item-wrap").each(function(){
            var showable = false;
            postcats = $(this).attr('data-cats').split(',');
            for(var i =0; i<postcats.length; i++){
                if(parseInt(postcats[i]) == parseInt(catid)){
                    showable = true;
                    break;
                }
            }
            if(showable == true) {
                $(this).attr('data-showable', 'true')
                $(this).attr('data-show','false')
                $(this).addClass('d-none'); 
            } else{
                $(this).attr('data-showable', 'false')
                $(this).attr('data-show','false')
                $(this).addClass('d-none'); 
            }
        });
        showbylimit();
    }
    function enablebyall(){
        $(".p-item-wrap").each(function(){
            $(this).attr('data-showable', 'true')
            $(this).attr('data-show','false')
            $(this).addClass('d-none');  
        });
        showbylimit();
    }
    function showbylimit(){
        let counter = 1;
        $(".p-item-wrap").each(function(){
            if(counter<=9){
                if($(this).attr('data-showable')=="true"){
                    if($(this).attr('data-show')=="false"){
                        $(this).removeClass('d-none'); 
                        $(this).attr('data-show','true')
                        counter++;
                    }
                }
            }
        });
        AOS.init();
    }
    function activeitem(e){
        $(".product-menu .nav-item").each(function(){
            $(this).removeClass('active')
        })
        $(e).addClass('active')
    }

    $(document).ready(()=>{
        enablebyall()      
    })
</script>