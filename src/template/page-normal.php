<?php
while (have_posts()) {
    the_post();
    ?>
    <div class='page-wrap'>
        <div class='page-wrap-box'>
            <!-- start page head -->
		    <?php get_template_part( 'src/template/partial', 'pagehead' ); ?>
            <!-- end page head -->
            <div class='page'>
                <div class='container'>
                    <div class='page-box'>
                        <!-- start page menu -->
		                <?php get_template_part( 'src/template/partial', 'pagemenu' ); ?>
                        <!-- end page menu -->
                        <div class='page-body'>
                            <div class='page-body-box'>
                                <div class='content'>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/icon-up-arrow.svg" class="upper" id="upper" alt="">


                    </div> <!-- end page-box -->
                </div> <!-- end container -->
            </div> <!-- end page -->
        </div> <!-- end page wrap box -->
    </div> <!-- end page wrap -->
    
    <?php
    wp_reset_postdata();
} //end while
?>