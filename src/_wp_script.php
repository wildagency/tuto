<?php
function add_theme_scripts()
{
    /** ==================================================== **/
    /*                 LIBRARIES                              */
    /** ==================================================== **/
    // jquery
    wp_deregister_script('jquery'); // deregisters the default WordPress jQuery
    wp_register_script('jquery', get_template_directory_uri() . '/assets/plugins/jquery/v3.3.1/jquery-3.3.1.min.js');
    wp_enqueue_script('jquery');
   
    // bootstrap
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/plugins/bootstrap/v4.3.1/bootstrap.min.css');
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/plugins/bootstrap/v4.3.1/bootstrap.bundle.min.js');

    // swiper
    wp_enqueue_style('swiper', get_template_directory_uri() . '/assets/plugins/swiper/v4.5.0/swiper.min.css');
    wp_enqueue_script('swiper', get_template_directory_uri() . '/assets/plugins/swiper/v4.5.0/swiper.min.js');

    // swiper
    wp_enqueue_style('aos', get_template_directory_uri() . '/assets/plugins/aos/v2.3.1/aos.css');
    wp_enqueue_script('aos', get_template_directory_uri() . '/assets/plugins/aos/v2.3.1/aos.js');

    // lightgallery
    wp_enqueue_style('lightgallery', get_template_directory_uri() . '/assets/plugins/lightgallery/dist/css/lightgallery.min.css');
    wp_enqueue_script('picturefill', get_template_directory_uri() . '/assets/plugins/lightgallery/dist/js/picturefill.min.js');
    wp_enqueue_script('lightgallery', get_template_directory_uri() . '/assets/plugins/lightgallery/dist/js/lightgallery-all.min.js');


    /** ==================================================== **/
    /*                CUSTOM STYLE & SCRIPTS                  */
    /** ==================================================== **/
    // css
    wp_enqueue_style('main', get_template_directory_uri() . '/assets/style/main.css');
    // js
    // wp_enqueue_script('loader', get_template_directory_uri() . '/assets/js/loader.js','','',true);
    wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js','','',true);
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');
