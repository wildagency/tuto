<?php

function wpb_custom_logo()
{
    echo '
    <style type="text/css">
        #wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
            background-image: url(http://www.wild.agency/wp-content/uploads/2018/12/logo.png) !important;
            position: absolute;
            width: 100%; height: 100%;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
            color:rgba(0, 0, 0, 0);
        }
        #wpadminbar #wp-admin-bar-wp-logo .ab-item .ab-icon {
            width: 50px !important;
        }
    </style>
    ';
}

//hook into the administrative header output
add_action('wp_before_admin_bar_render', 'wpb_custom_logo');

// update toolbar
function update_adminbar($wp_adminbar)
{

    // remove unnecessary items
    // $wp_adminbar->remove_node('about');
    // $wp_adminbar->remove_node('wporg');
    $wp_adminbar->remove_node('documentation');
    $wp_adminbar->remove_node('support-forums');
    // $wp_adminbar->remove_node('feedback');

    $wp_adminbar->remove_node('customize');
    $wp_adminbar->remove_node('comments');

    // Add "About WordPress" link
    $wp_adminbar->add_menu(array(
        'id' => 'wp-logo',
        'href' => 'http://wild.agency',
    ));
    // Add "About WordPress" link
    $wp_adminbar->add_menu(array(
        'parent' => 'wp-logo',
        'id' => 'about',
        'title' => __('About Wild'),
        'href' => 'http://www.wild.agency/about/',
    ));
    $wp_adminbar->add_menu(array(
        'parent' => 'wp-logo-external',
        'id' => 'wporg',
        'title' => __('Wild.agency'),
        'href' => 'http://wild.agency',
    ));
    $wp_adminbar->add_menu(array(
        'parent' => 'wp-logo-external',
        'id' => 'feedback',
        'title' => __('Contact'),
        'href' => 'http://www.wild.agency/contact/',
    ));
}

// admin_bar_menu hook
add_action('admin_bar_menu', 'update_adminbar', 999);



// Admin footer modification
function remove_footer_admin () 
{
    echo '<span id="footer-thankyou">by <a href="http://wild.agency" target="_blank">Wild Digital Agency</a></span>';
}
add_filter('admin_footer_text', 'remove_footer_admin');
