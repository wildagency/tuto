<?php
// cmb2, polylang, wpbakery

// cmb2
foreach (glob(get_template_directory() . '/src/cmb2/*.php') as $filename) {
    include_once $filename;
}

// polylang
foreach (glob(get_template_directory() . '/src/polylang/*.php') as $filename) {
    include_once $filename;
}

// wpbakery
add_action('vc_before_init', 'vc_before_init_actions');
function vc_before_init_actions()
{
    // Require new custom Element
    foreach (glob(get_template_directory() . '/src/wpbakery/*.php') as $filename) {
        include_once $filename;
    }
}
