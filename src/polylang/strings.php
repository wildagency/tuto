<?php

if (isset($polylang)) {
    function site_languages()
    {
        global $polylang;
        $Langs = $polylang->model->get_languages_list();
        return $Langs;
    }
    add_action('init', 'site_languages');
    $lang_args = array(
        array('description' => 'Page not found', 'keyboard' => 'notfound'),
        array('description' => 'All', 'keyboard' => 'all'),
        array('description' => 'History', 'keyboard' => 'history'),
        array('description' => 'Vision', 'keyboard' => 'vision'),
        array('description' => 'Mission', 'keyboard' => 'mission'),
        array('description' => 'Founders', 'keyboard' => 'founders'),
        array('description' => 'Awards', 'keyboard' => 'awards'),
        array('description' => 'ADC+ Консорциум', 'keyboard' => 'ads'),
        array('description' => 'Contact', 'keyboard' => 'contact'),
        array('description' => 'Address', 'keyboard' => 'address'),
        array('description' => 'Email', 'keyboard' => 'email'),
        array('description' => 'Phone', 'keyboard' => 'phone'),
        array('description' => 'Tel', 'keyboard' => 'tel'),
        array('description' => 'Workingday', 'keyboard' => 'workingday'),
        array('description' => 'Saturday', 'keyboard' => 'saturday'),
        array('description' => 'Sunday', 'keyboard' => 'sunday'),
        array('description' => 'Closed', 'keyboard' => 'closed'),
        array('description' => 'Send feedback', 'keyboard' => 'feedback'),
        array('description' => 'Client', 'keyboard' => 'client'),
        array('description' => 'Year', 'keyboard' => 'year'),
        
        array('description' => 'Location', 'keyboard' => 'location'),
        array('description' => 'Capacity', 'keyboard' => 'capacity'),
        array('description' => 'Floors', 'keyboard' => 'floors'),
        array('description' => 'All area', 'keyboard' => 'allarea'),
        array('description' => 'Building area', 'keyboard' => 'buildingarea'),
        array('description' => 'Empty area', 'keyboard' => 'emptyarea'),
        array('description' => 'Working area', 'keyboard' => 'workingarea'),
        array('description' => 'Structure', 'keyboard' => 'structure'),
        array('description' => 'Picture level', 'keyboard' => 'picturelevel'),

        array('description' => 'Text', 'keyboard' => 'text'),
        array('description' => 'Sketches', 'keyboard' => 'sketches'),
        array('description' => 'Plans', 'keyboard' => 'plans'),
        array('description' => 'Models', 'keyboard' => 'models'),
        array('description' => 'Final images', 'keyboard' => 'finalimages'),

        array('description' => 'Prev Thez', 'keyboard' => 'prevthez'),
        array('description' => 'Next Thez', 'keyboard' => 'nextthez'),
        
        
        array('description' => 'Thez`s', 'keyboard' => 'thezs'),
        array('description' => 'About', 'keyboard' => 'about'),

        array('description' => 'Download brochure', 'keyboard' => 'download_brochure'),
        array('description' => 'Team', 'keyboard' => 'team'),
        array('description' => 'Readmore', 'keyboard' => 'readmore'),

    );
    //Declare and set language strings to Global variables
    foreach ($lang_args as $key => $value) {
        pll_register_string($value['description'], $value['keyboard']);
    }
}
