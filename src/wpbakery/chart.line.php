<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class vcgchartjsLine extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_gchartjsline_mapping'));
        add_shortcode('vc_gchartjsline', array($this, 'vc_gchartjsline_html'));
    }

    // Element Mapping
    public function vc_gchartjsline_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Pie Chart', 'text-domain'),
                'base' => 'vc_gchartjsline',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'icon-wpb-vc-line-chart',
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => 'Line chart name',
                        'param_name' => 'mg_chartname',
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Rate name',
                        'param_name' => 'mg_ratename',
                    ),
                    array(
                        'type' => 'param_group',
                        'heading' => __('Data 1', 'my-text-domain'),
                        'param_name' => 'mg_data1',
                        // Note params is mapped inside param-group:
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'heading' => 'Line item name',
                                'param_name' => 'mg_name',
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => 'Line item alue',
                                'param_name' => 'mg_value',
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => 'Rate',
                                'param_name' => 'mg_extravalue',
                            ),
                        )
                    ),

                ),
            )
        );
    }

    // Element HTML
    public function vc_gchartjsline_html($atts)
    {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'mg_chartname' => '',
                    'mg_ratename' => '',
                    'mg_data1' => ''
                ), $atts
            )
        );
        $data1 = vc_param_group_parse_atts( $atts['mg_data1'] ); 
        
        $nameArgs1 = [];
        $valueArgs1 = [];
        $valueExtraArgs1 = [];
        $dataLength = 0;
        foreach ($data1 as $key => $value) {
            if(!empty($value['mg_name']) && !empty($value['mg_value'])){
                if(is_numeric($value['mg_value'])){
                    array_push($nameArgs1,$value['mg_name']);
                    array_push($valueArgs1,$value['mg_value']);
                    array_push($valueExtraArgs1,$value['mg_extravalue']);
                }
                $dataLength++;
            }
        }
        $nameArgs1 = json_encode($nameArgs1);
        $valueArgs1 = json_encode($valueArgs1);
        $valueExtraArgs1 = json_encode($valueExtraArgs1);
        $rand1 = mt_rand(10000,99999);

        $bgColor1 = json_encode(
            [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(25, 99, 132, 0.2)',
                'rgba(4, 162, 235, 0.2)',
                'rgba(5, 206, 86, 0.2)',
                'rgba(5, 192, 192, 0.2)',
                'rgba(53, 102, 255, 0.2)'
            ]
        );
        $brdrColor1 = json_encode(
            [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(25,99,132,1)',
                'rgba(4, 162, 235, 1)',
                'rgba(5, 206, 86, 1)',
                'rgba(5, 192, 192, 1)',
                'rgba(53, 102, 255, 1)',
            ]
        );

        $html = "
            <div class='card'>
                <div class='card-body'>
                    <div class='chart-body'>
                        <canvas id='chart$rand1'></canvas>
                    </div>
                </div>
            </div>
        
            <script>
                var ctx$rand1 = document.getElementById('chart$rand1').getContext('2d');
                var myChart$rand1 = new Chart(ctx$rand1, {
                    type: 'bar',
                    data: {
                        labels: $nameArgs1,
                        datasets: [{
                            type: 'bar',
                            label: '$mg_chartname',
                            data: $valueArgs1,
                            backgroundColor: $bgColor1,
                            borderColor: $brdrColor1,
                            borderWidth: 1,
                            yAxisID: 'y-axis-1'
                        }, {
                            type: 'line',
                            label: '$mg_ratename',
                            data: $valueExtraArgs1,
                            yAxisID: 'y-axis-2'
                        }]
                    },
                    options: {
                        responsive: true,
                        tooltips: {
                        },
                        elements: {
                            line: {
                                fill: false
                            }
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                gridLines: {
                                    display: false
                                },

                            }],
                            yAxes: [{
                                id: 'y-axis-1',
                                type: 'linear',
                                display: true,
                                position: 'left',
                                gridLines: {
                                    display: false
                                },
                                labels: {
                                    show: true,

                                }
                            }, {
                                id: 'y-axis-2',
                                type: 'linear',
                                display: true,
                                position: 'right',
                                gridLines: {
                                    display: false
                                },
                                labels: {
                                    show: true,

                                },
                                ticks: {
                                    min: 0,
                                    max: 100,
                                    callback: function (value) { return value + '%' }
                                },
                                scaleLabel: {
                                    display: true,
                                }
                            }]
                        }
                    }

                });
            </script>";
       
            

        return $html;
    }

}

// End Element Class
// Element Class Init
new vcgchartjsLine();
