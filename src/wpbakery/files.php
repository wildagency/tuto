<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class vcgfiles extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_gfiles_mapping'));
        add_shortcode('vc_gfiles', array($this, 'vc_gfiles_html'));
    }

    // Element Mapping
    public function vc_gfiles_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('File list', 'text-domain'),
                'base' => 'vc_gfiles',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'icon-wpb-application-icon-large',
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => 'List title',
                        'param_name' => 'mg_title',
                    ),
                    array(
                        'type' => 'param_group',
                        'heading' => __('Files', 'my-text-domain'),
                        'param_name' => 'mg_files',
                        // Note params is mapped inside param-group:
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'heading' => 'File name',
                                'param_name' => 'mg_name',
                            ),
                            array(
                                'type' => 'vc_link',
                                'heading' => 'File',
                                'param_name' => 'mg_file_url',
                            ),
                            array(
                                'type' => 'checkbox',
                                'heading' => 'For download',
                                'param_name' => 'mg_isdownload',
                            ),
                        )
                    ),

                ),
            )
        );
    }

    // Element HTML
    public function vc_gfiles_html($atts)
    {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'mg_title' => '',
                    'mg_files' => ''
                ), $atts
            )
        );
        $mg_files = vc_param_group_parse_atts( $atts['mg_files'] ); 
        
        $itemsHTML = "";
        foreach ($mg_files as $key => $value) {
            $mg_file_url = ($value['mg_file_url'] == '||') ? '' : $value['mg_file_url'];
            $mg_file_url = vc_build_link($mg_file_url)['url'];
            $is_download = $value['mg_isdownload'];
            $downloadHTML = ($is_download==true)?"download":"";

            $extension = end(explode(".", $mg_file_url));
            $itemsHTML .= "<a class='list-group-item d-flex justify-content-between align-items-center' href='$mg_file_url' target='_blank' $downloadHTML >
                            ".$value['mg_name']." <span class='badge'><img src='".esc_url(get_template_directory_uri())."/assets/images/icon-save.svg' alt=''>$extension</span>
                        </a>";
        }
        if(!empty($mg_title)){
            $titleHTML = "<li class='list-group-item list-group-head d-flex justify-content-between align-items-center'><strong>$mg_title</strong></li>";
        }
        $html = "<div class='card'>
                    <ul class='list-group report-list'>
                        $titleHTML
                        $itemsHTML
                    </ul>
                </div>";
       
        
        return $html;
    }

}

// End Element Class
// Element Class Init
new vcgfiles();
