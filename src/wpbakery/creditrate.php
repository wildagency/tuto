<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class vccreeditrate extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_creeditrate_mapping'));
        add_shortcode('vc_creeditrate', array($this, 'vc_creeditrate_html'));
    }

    // Element Mapping
    public function vc_creeditrate_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Credit rate', 'text-domain'),
                'base' => 'vc_creeditrate',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'icon-wpb-ui-creditrate',
                'params' => array(
                    array(
                        'type' => 'attach_images',
                        'heading' => __('Images', 'text-domain'),
                        'param_name' => 'mg_images',
                        'admin_label' => false,
                    ),
                    array(
                        'type' => 'param_group',
                        'heading' => __('Row', 'my-text-domain'),
                        'param_name' => 'mg_row',
                        // Note params is mapped inside param-group:
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'heading' => 'Title',
                                'param_name' => 'mg_title',
                            ),
                            array(
                                'type' => 'param_group',
                                'heading' => __('Row', 'my-text-domain'),
                                'param_name' => 'mg_rowchild',
                                // Note params is mapped inside param-group:
                                'params' => array(
                                    array(
                                        'type' => 'textfield',
                                        'heading' => 'Value',
                                        'param_name' => 'mg_value',
                                    ),
                                )
                            ),
                        )
                    ),

                ),
            )
        );
    }

    // Element HTML
    public function vc_creeditrate_html($atts)
    {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'mg_images' => '',
                    'mg_row' => ''
                ), $atts
            )
        );
        $imagIDs = explode(",", $mg_images);
        $imgCols = "";
        foreach ($imagIDs as $id) {
            $image_src = wp_get_attachment_image_src($id, 'thumbnail')[0];
            if(!empty($image_src)){
                $imgCols .= "<div class='col'><img src='".$image_src."' alt='' /></div>";
            }
        }
        $mg_row = vc_param_group_parse_atts( $atts['mg_row'] ); 
        $rowHTML ="";
        foreach ($mg_row as $key => $value) {
            $mg_rowchild = vc_param_group_parse_atts( $value['mg_rowchild'] ); 
            $rowHTML .="<li><div class='row'>";
            $rowHTML .="<div class='col-6'>".$value['mg_title']."</div>";
            foreach ($mg_rowchild as $val) {
                $rowHTML .="<div class='col'>".$val['mg_value']."</div>";
            }
            $rowHTML .="</div></li>";

        }
        if(!empty($imgCols)){
            $imgHTML = "<li class='head'>
                            <div class='row'>
                                <div class='col-6'></div>
                                $imgCols
                            </div>
                        </li>";
        }else{
            $imgHTML = "";
        }
       
        $html = "<div class='card'>
                    <div class='card-body'>
                        <ul class='credit-ratings'>
                            $imgHTML
                            $rowHTML
                        </ul>

                    </div>
                </div>";
        

       
       
    

        return $html;
    }

}

// End Element Class
// Element Class Init
new vccreeditrate();
