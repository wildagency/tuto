<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class vcgchartjs extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_gchartjs_mapping'));
        add_shortcode('vc_gchartjs', array($this, 'vc_gchartjs_html'));
    }

    // Element Mapping
    public function vc_gchartjs_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Pie Chart', 'text-domain'),
                'base' => 'vc_gchartjs',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'icon-wpb-vc_pie',
                'params' => array(
                    array(
                        'type' => 'checkbox',
                        'heading' => 'Show percentage',
                        'param_name' => 'mg_percentage',
                    ),
                    array(
                        'type' => 'param_group',
                        'heading' => __('Data', 'my-text-domain'),
                        'param_name' => 'mg_data',
                        // Note params is mapped inside param-group:
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'heading' => 'Name',
                                'param_name' => 'mg_name',
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => 'Value',
                                'param_name' => 'mg_value',
                            ),
                        )
                    ),

                ),
            )
        );
    }

    // Element HTML
    public function vc_gchartjs_html($atts)
    {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'mg_percentage' => '',
                    'mg_data' => ''
                ), $atts
            )
        );
        $data1 = vc_param_group_parse_atts( $atts['mg_data'] ); 
        
        $nameArgs1 = [];
        $valueArgs1 = [];
        $dataLength = 0;
        foreach ($data1 as $key => $value) {
            if(!empty($value['mg_name']) && !empty($value['mg_value'])){
                if(is_numeric($value['mg_value'])){
                    array_push($nameArgs1,$value['mg_name']);
                    array_push($valueArgs1,$value['mg_value']);
                }
                $dataLength++;
            }
        }
        $nameArgs1 = json_encode($nameArgs1);
        $valueArgs1 = json_encode($valueArgs1);
        $rand1 = mt_rand(10000,99999);

        $bgColor1 = json_encode(
            [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(25, 99, 132, 0.2)',
                'rgba(4, 162, 235, 0.2)',
                'rgba(5, 206, 86, 0.2)',
                'rgba(5, 192, 192, 0.2)',
                'rgba(53, 102, 255, 0.2)'
            ]
        );
        $brdrColor1 = json_encode(
            [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(25,99,132,1)',
                'rgba(4, 162, 235, 1)',
                'rgba(5, 206, 86, 1)',
                'rgba(5, 192, 192, 1)',
                'rgba(53, 102, 255, 1)',
            ]
        );

        if($mg_percentage==true){
            $showPercentageHTML = "callbacks: {
                                        label: function(tooltipItem, data) {
                                            var allData = data.datasets[tooltipItem.datasetIndex].data;
                                            var tooltipLabel = data.labels[tooltipItem.index];
                                            var tooltipData = allData[tooltipItem.index];
                                            
                                            var total = 0;
                                            for (var i in allData) {
                                                total += parseInt(allData[i]);
                                            }
                                            var tooltipPercentage = Math.round((tooltipData / total) * 100);
                                            return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
                                        }
                                    },";
        }else{
            $showPercentageHTML = "";
        }

        $html = "
            <div class='card'>
                <div class='card-body'>
                    <div class='chart-body'>
                        <canvas id='chart$rand1'></canvas>
                    </div>
                </div>
            </div>
        
            <script>
                var ctx$rand1 = document.getElementById('chart$rand1').getContext('2d');
                var myChart$rand1 = new Chart(ctx$rand1, {
                    type: 'pie',
                    data: {
                        labels: $nameArgs1,
                        datasets: [{
                            data: $valueArgs1,
                            backgroundColor: $bgColor1,
                            borderColor: $brdrColor1,
                            borderWidth: 1
                        }]
                    },
                    options: {
                        tooltips: {
                            $showPercentageHTML
                        },
                        legend: {
                            position: 'right',
                            labels: {
                                fontSize: 16,
                                fontStyle: 300,
                                padding: 15,
                                usePointStyle: true,
                                pointStyle: 'cross',
                            }
                        },
                        layout: {
                            padding: {
                                left: 0,
                                right: 50,
                                top: 35,
                                bottom: 35
                            }
                        }
                    },
                    point: {
                        pointStyle: 'cross'
                    }

                });
            </script>";
       
            

        return $html;
    }

}

// End Element Class
// Element Class Init
new vcgchartjs();
