<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class vcshareholders extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_shareholders_mapping'));
        add_shortcode('vc_shareholders', array($this, 'vc_shareholders_html'));
    }

    // Element Mapping
    public function vc_shareholders_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Share holders', 'text-domain'),
                'base' => 'vc_shareholders',
                'category' => __('Wild', 'text-domain'),
                // 'icon' => 'icon-wpb-application-icon-large',
                'params' => array(
                    array(
                        'type' => 'param_group',
                        'heading' => __('Holders', 'my-text-domain'),
                        'param_name' => 'mg_holders',
                        // Note params is mapped inside param-group:
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'heading' => 'Holder name',
                                'param_name' => 'mg_name',
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => 'Percentage',
                                'param_name' => 'mg_percentage',
                            ),
                        )
                    ),

                ),
            )
        );
    }

    // Element HTML
    public function vc_shareholders_html($atts)
    {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'mg_holders' => ''
                ), $atts
            )
        );
        $mg_holders = vc_param_group_parse_atts( $atts['mg_holders'] ); 

        $bgcolors = [
            'rgba(15, 109, 181, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(25, 99, 132, 1)',
            'rgba(4, 162, 235, 1)',
            'rgba(5, 206, 86, 1)',
            'rgba(5, 192, 192, 1)',
            'rgba(53, 102, 255, 1)'
        ];


        $ct = 0;
        $length = sizeof($mg_holders);
        $firstHalf = ($length-($length%2))/2+($length%2);
        $secondHalf = ($length-($length%2))/2;
        $listHTML = "<ul>";
        foreach ($mg_holders as $key => $value) {
            if($ct==$firstHalf){
                $listHTML .= "</ul><ul>";
            }
            $listHTML .= "<li>
                                <div class='list-style' style='background-color: ".$bgcolors[$ct]."'></div>
                                ".$value['mg_name']."
                                LLC (".$value['mg_percentage']."%)
                            </li>";

            


            $ct++;
        }
        $listHTML .= "</ul>";
        
        
        $GSymbolHTML = "";
        for ($i=sizeof($mg_holders)-1; $i>=0; $i--) { 
            $GSymbolHTML .= "<div class='' data-toggle='tooltip' data-placement='top' title='".$mg_holders[$i]['mg_name']."'
                            style='height: ".$mg_holders[$i]['mg_percentage']."%;background-color: ".$bgcolors[$i]."'>
                        </div>";
        }
    
        $html = "<div class='shareholders-wrap'>
                    <div class='card'>
                        <div class='card-body'>
                            <div class='shareholders'>
                                <div class='shareholders-box'>
                                    <div
                                        class='shareholders-logo d-flex justify-content-center'>
                                        <div class='shareholders-logo-mask'>
                                            <div class='colored-rows'>
                                               $GSymbolHTML
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class='shareholders-info d-flex align-items-center w-100'>
                                        <div
                                            class='shareholders-info-box d-flex flex-row align-items-center justify-content-between w-100'>
                                            $listHTML
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <script>
                    $(function () {
                        $('[data-toggle=\"tooltip\"]').tooltip()
                    })
                </script>";
       
        
        return $html;
    }

}

// End Element Class
// Element Class Init
new vcshareholders();
