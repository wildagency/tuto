<?php

class vcImage extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_Image2_mapping'));
        add_shortcode('vc_image2', array($this, 'vc_Image2_html'));
    }

    // Element Mapping
    public function vc_Image2_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Image', 'text-domain'),
                'base' => 'vc_image2',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'icon-wpb-single-image',
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => __('Image', 'text-domain'),
                        'param_name' => 'mg_image',
                        "holder" => "img",
                        'admin_label' => false,
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Image size', 'js_composer' ),
                        'param_name' => 'mg_size',
                        'value' => array(
                            __( 'Medium', 'js_composer' ) => 'medium',
                            __( 'Large', 'js_composer' ) => 'large',
                            __( 'Full', 'js_composer' ) => 'full',
                        ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Image style', 'js_composer' ),
                        'param_name' => 'mg_style',
                        'value' => array(
                            __( 'None', 'js_composer' ) => 'none',
                            __( 'Card', 'js_composer' ) => 'card'
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => 'Fit left and right (-40px)',
                        'param_name' => 'mg_fit',
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => 'Zoom effect',
                        'param_name' => 'mg_zoomer',
                    ),
                ),
            )
        );
    }

    // Element HTML
    public function vc_Image2_html($atts)
    {
        extract(
            shortcode_atts(
                array(
                    'mg_image' => '',
                    'mg_size' => '',
                    'mg_style' => '',
                    'mg_fit' => '',
                    'mg_zoomer' => ''
                ), $atts
            )
        );

        $rand = mt_rand(10000,99999);
        $mg_image_url = wp_get_attachment_image_src($mg_image, $mg_size)[0];
        
        $fitClass = ($mg_fit==true)?"fit-section":"";
        $cardClass = ($mg_style=="card")?"card":"";
        $cardBodyClass = ($mg_style=="card")?"card-body":"";
        if($mg_zoomer==true){
            // if zoom
            $mg_image_url_full = wp_get_attachment_image_src($mg_image, 'full')[0];
            $soomScriptHTML = "<script>
                            $(document).ready(function () {
                                $('#zoom_$rand').elevateZoom({
                                    zoomType: 'lens',
                                    lensShape: 'round',
                                    containLensZoom: true,
                                    lensSize: 200
                                })
                            })
                        </script>";
        }

        if(!empty($mg_image_url)){
            $html = "<div class='$cardClass $fitClass'>
                        <div class='$cardBodyClass p-0'>
                            <img id='zoom_$rand' src='$mg_image_url'
                                data-zoom-image='$mg_image_url_full'
                                class='w-100' />
                            $soomScriptHTML
                        </div>
                    </div>";
        }else{
            $html = "";
        }

        return $html;
    }

}

// End Element Class
// Element Class Init
new vcImage();
