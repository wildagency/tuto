<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class vcgPsloder extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_gPsloder_mapping'));
        add_shortcode('vc_gPsloder', array($this, 'vc_gPsloder_html'));
    }

    // Element Mapping
    public function vc_gPsloder_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        $getPostCats = get_terms( 'category', array(
            'hide_empty' => false,
        ) );
        $cats = array(__( 'All', 'textdomain' ) => 'all');
        foreach ($getPostCats as $key => $value) {
            $tid = $value->term_id;
            $name = $value->name;
            $cats = array_merge($cats, array(__( $name, 'textdomain' ) => "0$tid"));
        }


        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Post slider', 'text-domain'),
                'base' => 'vc_gPsloder',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'icon-wpb-slideshow',
                'params' => array(
                    array(
                        'type'        => 'dropdown',
                        'heading'     => __('Category'),
                        'param_name'  => 'mg_category',
                        'value'       => $cats,
                        'std'         => 'all', // Your default value
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => __('Limit'),
                        'param_name'  => 'mg_limit',
                        'admin_label' => true,
                        'value'       => array(
                          '3'   => '3',
                          '4'   => '4',
                          '5'   => '5',
                          '6'   => '6',
                          '7'   => '7',
                          '8'   => '8',
                        ),
                        'std'         => '3', // Your default value
                    ),

                ),
            )
        );
    }

    // Element HTML
    public function vc_gPsloder_html($atts)
    {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'mg_category' => '',
                    'mg_limit' => ''
                ), $atts
            )
        );       
        $mg_limit = (empty($mg_limit))?3:$mg_limit;
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => $mg_limit
        );
        if(!empty($mg_category) && $mg_category!="all"){
            $category_id = substr($mg_category, 1);
            $args = array_merge($args, array('cat' => $category_id));
        }
        // The Query
        
        $the_query = new WP_Query( $args );

        // The Loop
        $slides = "";
        $rand = mt_rand(10000,99999);
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'news-thumb');
                if(!empty($thumbnail_url)){
                    $imgHTML = "<img src='$thumbnail_url' class='card-img-top' alt='...'>";
                }
                $slides .= "<a href='".get_the_permalink()."' class='card swiper-slide'>
                                    $imgHTML
                                    <div class='card-body'>
                                        <h5 class='card-title'>".get_the_title()."</h5>
                                        <span href='#' class='btn btn-link'>".pll__('readmore')." <img
                                                src='".esc_url(get_template_directory_uri())."/assets/images/icon-arrow-right.svg'
                                                alt='' srcset='' class='symbol'></span>
                                    </div>
                                </a>";
            }
            /* Restore original Post Data */
            wp_reset_postdata();
        } else {
            // no posts found
        }

        $html = "<div class='swiper-container news-swiper slider$rand'>
                    <div class='swiper-wrapper'>$slides</div>
                    <div class='swiper-pagination'></div>
                </div>
                <script>
                    var swiper = new Swiper('.slider$rand', {
                        pagination: {
                            el: '.swiper-pagination',
                            preventClicks: false,
                            preventClicksPropagation: true
                        },
                        spaceBetween: 30,
                    });
                </script>";
        

       
       

        return $html;
    }

}

// End Element Class
// Element Class Init
new vcgPsloder();
