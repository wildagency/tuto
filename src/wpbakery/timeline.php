<?php

class vcTimeline extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_Timeline2_mapping'));
        add_shortcode('vc_Timeline2', array($this, 'vc_Timeline2_html'));
    }

    // Element Mapping
    public function vc_Timeline2_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Timeline', 'text-domain'),
                'base' => 'vc_Timeline2',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'icon-wpb-single-Timeline',
                'params' => array(
                    // array(
                    //     'type' => 'dropdown',
                    //     'heading' => __( 'Timeline size', 'js_composer' ),
                    //     'param_name' => 'mg_size',
                    //     'value' => array(
                    //         __( 'Medium', 'js_composer' ) => 'medium',
                    //         __( 'Large', 'js_composer' ) => 'large',
                    //         __( 'Full', 'js_composer' ) => 'full',
                    //     ),
                    // ),
                ),
            )
        );
    }

    // Element HTML
    public function vc_Timeline2_html($atts)
    {
        extract(
            shortcode_atts(
                array(
                    // 'mg_size' => '',
                ), $atts
            )
        );

        $args = array(
            'post_type' => 'timeline',
            'posts_per_page' => -1,
            'meta_key' => 'timeline_date',
            'orderby' => 'meta_value_num',
            'order' => 'DESC'

        );
        $data= [];
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                // $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'news-thumb');
                $year = get_post_meta(get_the_ID(),'timeline_date')[0];
                $title = get_the_title();
                $text = get_post_meta(get_the_ID(),'timeline_text')[0];
                $image_url = wp_get_attachment_image_src(get_post_meta(get_the_ID(),'timeline_image_id')[0], 'logo-thumb')[0];

                array_push($data, array(
                    'year' => $year, 
                    'title' => $title, 
                    'text' => $text, 
                    'image_url' => $image_url 
                ));
            }
            /* Restore original Post Data */
            wp_reset_postdata();
        } else {
            // no posts found
        }
        $maxYear = $data[0]['year'];
        $minYear = $data[sizeof($data)-1]['year'];

        $timeline = array();
        $temp = array();
        $odd = $maxYear%5;
        $downYear = $maxYear + (5-$odd);
        // $increaser = 5;
        // $prevOdd =5;
        foreach ($data as $key => $value) {
            if($value['year'] >= $downYear-5){
                array_push($temp,$value);
            }else{
                $maxY = $downYear-1;
                $minY = $downYear-5;
                array_push($timeline,array(
                    'max' => $maxY,
                    'min' => $minY,
                    'data' => $temp
                ));

                $downYear = $downYear-5;
                $temp = array();
                array_push($temp,$value);
                
            }
            if($value['year']==$minYear){
                $maxY = $downYear-1;
                $minY = $downYear-5;
                array_push($timeline,array(
                    'max' => $maxY,
                    'min' => $minY,
                    'data' => $temp
                ));
            }
        }
        $timeline[0]['max'] = intval($maxYear);
        $timeline[sizeof($timeline)-1]['min'] = intval($minYear);
        // var_dump($timeline);
        $rand = mt_rand(10000,99999);
        foreach($timeline as $key => $value){
            $tlTopSlidesHTML .= "<div class='swiper-slide'>
                                <div class='timeline-thumb-item'>
                                    ".$value['max']." - ".$value['min']."
                                </div>
                            </div>";
            $tlItems="";
            foreach ($value['data'] as $val) {
                $tlItems .= "<div class='col-lg-6'>
                                <div class='timeline-content'>
                                    <div class='timeline-content-item'>
                                        <div class='row'>
                                            <div class='col-sm-3'>
                                                <div
                                                    class='clogo-wrap award bordered  d-flex align-items-center justify-content-center'>
                                                    <img src='".$val['image_url']."'
                                                        alt='' srcset=''>
                                                </div>
                                            </div>
                                            <div class='col'>
                                                <strong>
                                                    <div class='tci-year'>".$val['year']."</div>".$val['title']."
                                                </strong>
                                                <p>".$val['text']."</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>";
            }
            $tlBotSlidesHTML .= "<div class='swiper-slide'>
                                    <div class='row'>
                                        $tlItems
                                    </div>
                                </div>";              
        }   
       
        $html = "   <div class='swiper-container timeline-thumb gallery-thumbs tltumb$rand'>
                        <div class='swiper-wrapper'>
                            $tlTopSlidesHTML
                        </div>
                    </div>
                    <div class='swiper-container gallery-top tltop$rand'>
                        <div class='swiper-wrapper'>
                            $tlBotSlidesHTML
                        </div>
                    </div>
                    <script>
                        var tltumb$rand = new Swiper('.tltumb$rand', {
                            spaceBetween: 0,
                            slidesPerView: 3,
                            loop: false,
                            freeMode: false,
                            loopedSlides: 3,
                            breakpoints: {
                                768: {
                                    slidesPerView: 2,
                                },
                                320: {
                                  slidesPerView: 1,
                                  spaceBetween: 10,
                                }
                            }
                        });
                        var tltop$rand = new Swiper('.tltop$rand', {
                            spaceBetween: 10,
                            loop: false,
                            // loopedSlides: 6, 
                            thumbs: {
                                swiper: tltumb$rand,
                            },
                        });
                    </script>";
        

        return $html;
    }

}

// End Element Class
// Element Class Init
new vcTimeline();
