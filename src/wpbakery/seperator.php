<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class vcSeperator extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_seperator_mapping'));
        add_shortcode('vc_seperator', array($this, 'vc_seperator_html'));
    }

    // Element Mapping
    public function vc_seperator_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Separator', 'text-domain'),
                'base' => 'vc_seperator',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'icon-wpb-ui-separator',
            )
        );
    }

    // Element HTML
    public function vc_seperator_html($atts)
    {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                ), $atts
            )
        );

		
        $html = "<div class='liner'></div>";

        return $html;
    }

}

// End Element Class
// Element Class Init
new vcSeperator();
