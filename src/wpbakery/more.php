<?php

class vcMore extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_more2_mapping'));
        add_shortcode('vc_more2', array($this, 'vc_more2_html'));
    }

    // Element Mapping
    public function vc_more2_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('More', 'text-domain'),
                'base' => 'vc_more2',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'icon-wpb-ui-separator-label',
                // 'icon' => get_template_directory_uri() . '/img/assets/borderbox.png',
                'params' => array(
                    array(
                        "type" => "textarea_html",
                        "holder" => "div",
                        "heading" => "More text",
                        "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
                        "value" => ""
                        //   'admin_label' => true,
                    ),
                ),
            )
        );
    }

    // Element HTML
    public function vc_more2_html($atts, $content = null)
    {
        shortcode_atts(
            array(
            ), $atts
        );
        $content = wpb_js_remove_wpautop($content, true);
        
        $rand = mt_rand(10000,99999);

        if(!empty($content)){
            $html = "<div class='custom-toggle'>
                        <div class='collapse' id='ce$rand'>
                            $content
                        </div>
                        <a class='btn btn-customtoggler' data-toggle='collapse'
                            href='#ce$rand' role='button' aria-expanded='false'
                            aria-controls='ce$rand'>
                            ".pll__('readmore')." <div class='closer'><span></span><span></span></div>
                        </a>
                    </div>";
        }else{
            $html = "";
        }

        return $html;
    }

}

// End Element Class
// Element Class Init
new vcMore();
