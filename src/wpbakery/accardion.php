<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class vcgAccardion extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_gaccardion_mapping'));
        add_shortcode('vc_gaccardion', array($this, 'vc_gaccardion_html'));
    }

    // Element Mapping
    public function vc_gaccardion_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Accordion', 'text-domain'),
                'base' => 'vc_gaccardion',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'icon-wpb-ui-accordion',
                'params' => array(
                    array(
                        'type'        => 'dropdown',
                        'heading'     => __('Style'),
                        'param_name'  => 'mg_style',
                        'admin_label' => true,
                        'value'       => array(
                          'none'   => 'None',
                          'Card'   => 'Card',
                        ),
                        'std'         => 'none', // Your default value
                    ),
                    array(
                        'type' => 'param_group',
                        'heading' => __('Button', 'my-text-domain'),
                        'param_name' => 'mg_accardion',
                        // Note params is mapped inside param-group:
                        'params' => array(
                            array(
                                'type' => 'attach_image',
                                'heading' => __('Icon', 'text-domain'),
                                'param_name' => 'mg_icon',
                                'admin_label' => false,
                            ),
                            array(
                                'type' => 'textfield',
                                'value' => '',
                                'heading' => 'Title',
                                'param_name' => 'mg_title',
                            ),
                            array(
                                'type' => 'textfield',
                                'value' => '',
                                'heading' => 'Title description',
                                'param_name' => 'mg_title_desc',
                            ),
                            array(
                                'type' => 'attach_image',
                                'heading' => __('Content image', 'text-domain'),
                                'param_name' => 'mg_image',
                                'admin_label' => false,
                            ),
                            array(
                                'type' => 'textarea',
                                'value' => '',
                                'heading' => 'Content',
                                'param_name' => 'mg_text',
                            ),
                            array(
                                'type' => 'checkbox',
                                'value' => '',
                                'heading' => 'Not list',
                                "description"   => "Content text is not list item",
                                'param_name' => 'notlister',
                            ),
                        )
                    ),

                ),
            )
        );
    }

    // Element HTML
    public function vc_gaccardion_html($atts)
    {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'mg_style' => '',
                    'mg_accardion' => ''
                ), $atts
            )
        );
		$accardion = vc_param_group_parse_atts( $atts['mg_accardion'] ); 

        $randID = mt_rand(10000, 99999);
        $cardsHTML = "";
        $count = 1;
        foreach ($accardion as $key => $value) {
            $icon_src = wp_get_attachment_image_src($value['mg_icon'], 'thumbnail')[0];
            $image_src = wp_get_attachment_image_src($value['mg_image'], 'thumbnail')[0];
            $iconHTML = (!empty($icon_src)) ? "<img src='$icon_src' alt='' />" : "";
            
            $title = $value['mg_title'];
            $titleDesc = $value['mg_title_desc'];
            $notlister = $value['notlister'];
            if($notlister==true){
                $text = $value['mg_text'];
            }else{
                $textLi = "";
                $textArr = explode("\n", $value['mg_text']);
                foreach ($textArr as $val) {
                    $textLi .= (!empty(trim($val)))?"<li>$val</li>":"";
                }
                $text = "<ul>$textLi</ul>";
            }
            $titleDescHTML = (!empty($titleDesc)) ? "<div class='pos'>$titleDesc</div>" : ""; 

            if(!empty($image_src)){
                $contentHTML = "<div class='row'>
                                    <div class='col'>
                                        <p class='card-text'>$text</p>
                                    </div>
                                    <div class='col-sm-5 col-md-3 text-sm-right'>
                                        <img src='$image_src' class='w-100' style='width: auto; max-width: 160px;' alt='' />
                                    </div>
                                </div>";
            }else{
                $contentHTML = "<p class='card-text'>$text</p>";
            }
            
          
            $cardsHTML .= "
                        <div class='card '>
                            <div class='card-header' id='h$randID$count'>
                                <div class='accordion-head' data-toggle='collapse'
                                    data-target='#c$randID$count' aria-expanded='false'
                                    aria-controls='c$randID$count'>
                                    <h5 class='card-title'>
                                        $iconHTML
                                        <div class='name'>$title</div>
                                        $titleDescHTML
                                    </h5>
                                    <div class='closer'>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                            <div id='c$randID$count' class='collapse'
                                aria-labelledby='h$randID$count'
                                data-parent='#accrodion$randID'>
                                <div class='card-body'>
                                    $contentHTML
                                </div>
                            </div>
                        </div>";
            $count++;
        }

        if($mg_style == "Card"){
            $html = "
            <div class='card award'>
                <div class='card-body'>
                    <div class='accordion' id='accrodion$randID'>
                        $cardsHTML
                    </div>
                </div>
            </div>";
        }else{
            $html = "
                <div class='award'>
                    <div class='accordion' id='accrodion$randID'>
                        $cardsHTML
                    </div>
                </div>";
        }

       
       
        // if(!empty($mg_image_url) || !empty($mg_title)){
        //     $html = "<a href='$btn_link' class='card' target='".trim($mg_readmore_url['target'])."' rel='".trim($mg_readmore_url['rel'])."' >
        //                 $image_html
        //                 <div class='card-body'>
        //                     <h5 class='card-title'>$mg_title</h5>
        //                     $btn_html
        //                 </div>
        //             </a>";
        // }

        return $html;
    }

}

// End Element Class
// Element Class Init
new vcgAccardion();
