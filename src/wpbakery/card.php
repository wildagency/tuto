<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class vcBoxContent extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_borderedbox_mapping'));
        add_shortcode('vc_borderedbox', array($this, 'vc_borderedbox_html'));
    }

    // Element Mapping
    public function vc_borderedbox_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Box content', 'text-domain'),
                'base' => 'vc_borderedbox',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'vc_icon-vc-hoverbox',
                // 'icon' => get_template_directory_uri() . '/img/assets/borderbox.png',
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => __('Image', 'text-domain'),
                        'param_name' => 'mg_image',
                        'admin_label' => false,
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Large image', 'text-domain'),
                        'param_name' => 'mg_image_large',
                        'admin_label' => false,
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Title',
                        'param_name' => 'mg_title',
                        'admin_label' => true,
                    ),
                    array(
                        'type' => 'vc_link',
                        'heading' => __('Button', 'my-text-domain'),
                        'param_name' => 'mg_readmore_url',
                    ),

                ),
            )
        );
    }

    // Element HTML
    public function vc_borderedbox_html($atts)
    {

        // Params extraction
        extract(
            shortcode_atts(
                array(
                    'mg_image' => '',
                    'mg_image_large' => '',
                    'mg_title' => '',
                    'mg_readmore_url' => '',
                ), $atts
            )
        );
        $thumbsize = ($mg_image_large==true)?'large':'medium';

		$mg_image_url = wp_get_attachment_image_src($mg_image, $thumbsize)[0];
		
		$mg_readmore_url = ($mg_readmore_url == '||') ? '' : $mg_readmore_url;
        $mg_readmore_url = vc_build_link($mg_readmore_url);
        $btn_link = (!empty($mg_readmore_url['url'])) ? $mg_readmore_url['url'] : "#_";
        $btn_title = ($mg_readmore_url['title'] == '') ? '' : $mg_readmore_url['title'];
        // $btn = (!empty($btn_link) && !empty($btn_title)) ? "<a href='$btn_link' class='btn btn-block btn-orange link'>$btn_title <img src='" . get_template_directory_uri() . "/assets/img/arrow-right-white.png' alt='' /></a>" : "";


        if (!empty($mg_image_url)) {
            $image_html = "<img src='$mg_image_url' class='card-img-top' alt='...'>";
        }
        if (!empty($btn_title)) {
            $btn_html = "<span class='btn btn-link'>$btn_title <img src='".esc_url(get_template_directory_uri())."/assets/images/icon-arrow-right.svg' alt='' srcset='' class='symbol'></span>";
        }

       
        if(!empty($mg_image_url) || !empty($mg_title)){
            $html = "<a href='$btn_link' class='card' target='".trim($mg_readmore_url['target'])."' rel='".trim($mg_readmore_url['rel'])."' >
                        $image_html
                        <div class='card-body'>
                            <h5 class='card-title'>$mg_title</h5>
                            $btn_html
                        </div>
                    </a>";
        }

        return $html;
    }

}

// End Element Class
// Element Class Init
new vcBoxContent();
