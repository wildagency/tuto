<?php

class vclogo2 extends WPBakeryShortCode
{

    // Element Init
    public function __construct()
    {
        add_action('init', array($this, 'vc_logo2_mapping'));
        add_shortcode('vc_logo2', array($this, 'vc_logo2_html'));
    }

    // Element Mapping
    public function vc_logo2_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('logo', 'text-domain'),
                'base' => 'vc_logo2',
                'category' => __('Wild', 'text-domain'),
                'icon' => 'icon-wpb-single-logo',
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => __('logo', 'text-domain'),
                        'param_name' => 'mg_logo',
                        "holder" => "img",
                        'admin_label' => false,
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Logo size', 'js_composer' ),
                        'param_name' => 'mg_size',
                        'value' => array(
                            __( 'Logo', 'js_composer' ) => 'logo',
                            __( 'Medium', 'js_composer' ) => 'medium',
                            __( 'Large', 'js_composer' ) => 'large',
                            __( 'Full', 'js_composer' ) => 'full',
                        ),
                    ),
                ),
            )
        );
    }

    // Element HTML
    public function vc_logo2_html($atts)
    {
        extract(
            shortcode_atts(
                array(
                    'mg_logo' => '',
                    'mg_size' => '',
                ), $atts
            )
        );
        $mg_size = ($mg_size=="logo" || $mg_size=="")?"logo-thumb":$mg_size;
        $mg_logo_url = wp_get_attachment_image_src(intval($mg_logo), $mg_size)[0];
        
   

        if(!empty($mg_logo_url)){
            $html = "<div class='clogo-wrap bordered'>
                        <img src='$mg_logo_url' alt='' srcset='' />
                    </div>";
        }else{
            $html = "";
        }

        return $html;
    }

}

// End Element Class
// Element Class Init
new vclogo2();
