<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0' />
    <meta http-equiv='X-UA-Compatible' content='ie=edge' />

    <title><?php wp_title('|', true, 'right');?></title>

    <!-- fav -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <?php wp_head();?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145632448-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-145632448-1');
    </script>

</head>


<body>


<?php 
if($_SERVER['REQUEST_URI']=='/'.pll_current_language('slug').'/about/'){
    $aboutActiveCls = "active";
}else{
    $homeActiveCls = "active";
}
$otherlangs = pll_the_languages(array(
    'raw'=>1,
    'hide_if_empty'=>0,
    'hide_current'=>1,
));
?>
<!-- Start Header -->
<div class='header-wrap active'>
    <div class='header-line'></div>
    <div class='container-wide'>
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/">
                <?php if(pll_current_language('slug')=='mn'){ ?>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/logo3.svg" alt="" srcset="">
                <?php } else { ?>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/logo4.svg" alt="" srcset="">
                <?php } ?>
            </a>
            <button class="navbar-toggler" id="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class='collapse2 navbar2' id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item <?php echo $homeActiveCls; ?>">
                        <a class="nav-link" href="/"><?php echo pll__('thezs'); ?></a>
                    </li>
                    <li class="nav-item <?php echo $aboutActiveCls; ?>">
                        <a class="nav-link" href="/<?php echo pll_current_language('slug'); ?>/about/"><?php echo pll__('about'); ?></a>
                    </li>
                </ul>
                <ul class="navbar-nav lang">
                    <?php foreach($otherlangs as $key => $value){?>
                        <li class="nav-item">
                            <a href="/<?php echo $value['slug'];?>" class="nav-link" ><?php echo $value['name'];?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            
            <div class="collapse navbar-collapse" >
                <div class='w-100 d-flex align-items-end'>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item <?php echo $homeActiveCls; ?>">
                            <a class="nav-link" href="/"><?php echo pll__('thezs'); ?></a>
                        </li>
                        <li class="nav-item <?php echo $aboutActiveCls; ?>">
                            <a class="nav-link" href="/<?php echo pll_current_language('slug'); ?>/about/"><?php echo pll__('about'); ?></a>
                        </li>
                    </ul>
                    <ul class="navbar-nav lang">
                        <?php foreach($otherlangs as $key => $value){?>
                            <li class="nav-item">
                                <a href="/<?php echo $value['slug'];?>" class="nav-link" ><?php echo $value['name'];?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>

<!-- End Header -->
<!-- Start Body -->

